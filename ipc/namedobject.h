#ifndef IPC_NAMED_OBJECT_H
#define IPC_NAMED_OBJECT_H

/*! \file object.h Defines base classes for IPC objects.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <ipc/partition.h>
#include <ipc/objectbase.h>
#include <ipc/servant.h>

/*! \brief Base class for named IPC objects.

    Custom classes shall inherits from this one for been able to register their instances to the
    Naming Service.
    Template parameters:
	- class T is a class which implements a certain interface which has been
	defined in IDL file. This class is generated by the idl2c++ compiler.
	- class TP (Thread Policy) - this class defines some details of this object implementation
	    The possibilities are:
		- ipc::multi_thread - requests can be executed in multiple threads concurrently
		- ipc::single_thread - requests will be serialized and executed in the context
			of a single thread
	- class PP (Persistence Policy) - this class defines some details of this object implementation
	    The possibilities are:
		- ipc::transient - transient object
		- ipc::single_thread - persistent object
                        
    \attention	Object of any class which inherits from this one must be allocated on heap using the new
		operator and must be destroyed via calling the _destroy() function. The delete operator
		must never be used for that purpose.
                
    \ingroup public
    \sa IPCObject
 */
template <class T, class TP = ipc::multi_thread, class PP = ipc::persistent>
class IPCNamedObject :	public IPCObjectBase<T,TP,PP>,
			public IPCServant
{
  public:
        
    virtual ~IPCNamedObject ( )
    { ; }
    
    /*! Returns the name of this object. This name is used to address this object
    	in the context of the partition which it belongs to.
        \return object's name.
     */
    const std::string & name ( ) const {
	return m_name;
    }
    
    /*! Returns the partition which this object belongs to.
        \return object's partition.
     */
    const IPCPartition & partition ( ) const {
	return m_partition;
    }
        
    /*! 
     * Publishes this object to the IPC partition.
     * \throw  daq::ipc::InvalidPartition
     * \throw  daq::ipc::InvalidObjectName
     */
    void publish ( ) {
    	ipc::servant_var ref = _this();
    	partition().publish( name(), ipc::util::getTypeName( this ), ref );
    }
    
    /*! 
     * Removes the publication of this object from the IPC partition.
     * \throw  daq::ipc::InvalidPartition
     * \throw  daq::ipc::InvalidObjectName
     * \throw  daq::ipc::ObjectNotFound
     */
    void withdraw ( ) {
	partition().withdraw( name(), ipc::util::getTypeName( this ) );
    }
    
  protected:
    /*! Constructs new object in the default partition.
    	In order to register this object with the target partition one should use
        the IPCNamedObject::publish() method.
        \param  name 		object's name
     */
    IPCNamedObject ( const std::string & name )
      : IPCServant(IPCPartition(), name),
        m_name( name )
    { ; }

    /*! Constructs new object in the given partition.
    	In order to register this object with the given partition one should use
        the IPCNamedObject::publish() method.
        \param  partition 	object's partition
        \param  name 		object's name
     */
    IPCNamedObject ( const IPCPartition & partition,
		     const std::string & name )
      : IPCServant(partition, name),
      	m_partition( partition ),
	m_name( name )
    { ; }

    /*! Constructs new object in the given partition.
    	This object will be synchronized with another named IPC object in a sense that all remote
        methods for those objects will be executed sequentially.
        \param  partition 	object's partition
        \param  name 		object's name
        \param  object 		the newly created object will be synchronized with this one
     */
    IPCNamedObject ( const IPCPartition & partition,
		     const std::string & name,
                     const IPCNamedObject * object )
      : IPCObjectBase<T,TP,PP>( object ),
      	IPCServant(partition, name),
        m_partition( partition ),
	m_name( name )
    { ; }
    
    std::string unique_id() const {
    	return (m_partition.name() + '/' + ipc::util::getTypeName( this ) + '/' + m_name);
    }

  protected:
    const IPCPartition	m_partition;
    const std::string	m_name;

  private:
    ////////////////////////
    // Disable copying
    ////////////////////////
    IPCNamedObject ( const IPCNamedObject & );
    IPCNamedObject & operator= ( const IPCNamedObject & );
};

#endif
