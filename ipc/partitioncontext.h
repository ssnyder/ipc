#ifndef IPC_PARTITION_CONTEXT_H
#define IPC_PARTITION_CONTEXT_H

///////////////////////////////////////////////////////////////////////////
//      partitioncontext.h
//
//      header file for the IPC library
//
//      Sergei Kolos, June 2013
//
//      description:
//        IPCPartitionContext - helper class for dealing with remote object context
///////////////////////////////////////////////////////////////////////////
#include <string>

#include <ipc/ipc.hh>

struct IPCPartitionContext : protected ipc::servant::PartitionContext_var
{
    IPCPartitionContext( ipc::servant_ptr entity );
    IPCPartitionContext( POA_ipc::servant & entity );
    
    const bool		m_valid;
    const std::string	m_partition;
    const std::string	m_name;

private:
    IPCPartitionContext(ipc::servant::PartitionContext * p);
};

#endif
