/////////////////////////////////////////////////////////////////////////////////////////
//      objectbase.h
//
//      Implementation of the IPC proxy application
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains implementation of the IPC proxy object interface
//
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef IPC_PROXY_OBJECT_BASE_H
#define IPC_PROXY_OBJECT_BASE_H

//////////////////////////////////////////////////////////////////////////////////
// The underscores are used to avoid clashes of the names of these methods with
// the names of the methods defined for a particular IDL interface
//////////////////////////////////////////////////////////////////////////////////
class IPCProxyObjectBase
{
public:
    virtual ~IPCProxyObjectBase()
    {
	;
    }

    virtual void __destroy__() = 0;
    virtual CORBA::Object_ptr __reference__() = 0;
    virtual bool __target_exists__() = 0;
};

#endif
