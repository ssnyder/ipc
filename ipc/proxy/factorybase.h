////////////////////////////////////////////////////////////////////////////
//      factorybase.h
//
//      Implementation of the IPC proxy application
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains implementation of the IPC proxy factory
//
////////////////////////////////////////////////////////////////////////////

#ifndef IPC_PROXY_FACTORY_BASE_H
#define IPC_PROXY_FACTORY_BASE_H

#include <string>

#include <ipc/partition.h>
#include <ipc/proxy/objectbase.h>

class IPCProxyFactoryBase
{
  public:
    
    virtual ~IPCProxyFactoryBase() { ; }
    
    virtual IPCProxyObjectBase * create( const std::string & ior ) = 0;
    
    virtual IPCProxyObjectBase * create( const IPCPartition & partition,
            const std::string & name, CORBA::Object_ptr remote ) {
        return nullptr;
    }

    virtual const std::string & interfaceId( ) = 0;
};

#endif
