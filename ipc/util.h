#ifndef IPC_UTIL_H
#define IPC_UTIL_H

////////////////////////////////////////////////////////////////////////////
//      util.h
//
//      header file for the IPC library
//
//      Sergei Kolos August 2006
//
//    description:
//        This file contains a set of utility functions for the IPC package.
//        
////////////////////////////////////////////////////////////////////////////

#include <omniORB4/CORBA.h>
#include <string>
#include <vector>

#include <partition/partition.hh>
#include <ipc/exceptions.h>

/*! \brief Main namespace of the IPC package
 \ingroup public
 \sa
 */
namespace ipc
{
    /*! \brief Namespace for the IPC internal utility functions
     \ingroup private
     \sa
     */
    namespace util
    {
	//!
	std::string
	extractTypeName(const std::string & object_name) throw ();

	//!
	void
	bind(const std::string & partition_name,
		const std::string & object_name, const std::string & type_name,
		CORBA::Object_ptr obj);

	std::string
	constructReference(const std::string & partition_name,
		const std::string & object_name,
		const std::string & type_name,
		const std::vector<std::pair<std::string, int> > & endpoints) throw ();

	std::string
	constructReference(const std::string & object_name,
		const std::string & type_name,
		const std::vector<std::pair<std::string, int> > & endpoints) throw ();

	std::string
	constructReference(const std::string & object_name,
		const std::string & type_name,
		const std::string & file_name) throw ();

	void
	createContext(ipc::partition_ptr nc, const std::string & context_name);

	CosNaming::NamingContext_ptr
	findContext(ipc::partition_ptr nc, const std::string & context_name);

	const std::string &
	getInitialReference() throw ();

	std::string
	getTypeName(const PortableServer::ServantBase * servant) throw ();

	/*! Extracts object's type name from the CORBA object type T
	 \return type name for the given class
	 \throw  Never throws exceptions
	 */
	template<class T>
	std::string getTypeName() throw ()
	{
	    return extractTypeName(T::_PD_repoId);
	}

	void
	list(const std::string & partition_name, const std::string & type_name,
		CosNaming::BindingList_var & list);

	CORBA::Object_ptr
	resolve(const std::string & partition_name,
		const std::string & object_name, const std::string & type_name);

	ipc::partition_ptr
	getPartition(const std::string & partition_name);

	CORBA::Object_ptr
	resolvePartition(const std::string & partition_name);

	CORBA::Object_ptr
	resolveInitialReference(const std::string & ref);

        std::string
        getInitialObjectReference();

	void
	storeAsInitialReference(const std::string & reference);

	void
	unbind(const std::string & partition_name,
		const std::string & object_name, const std::string & type_name);

	bool
	isValidChar(int c);
    }
}

#endif
