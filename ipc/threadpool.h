#ifndef IPC_THREAD_POOL_H
#define IPC_THREAD_POOL_H

////////////////////////////////////////////////////////////////////////////
//
//     threadpool.h
//
//     source file for IPC library
//
//     Sergei Kolos June 2003
//
//     description:
//       This file contains declaration of the IPCThreadPool class
//
////////////////////////////////////////////////////////////////////////////

#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <boost/function.hpp> 
#include <queue>

class IPCThreadPool : boost::noncopyable
{
  public:
    typedef boost::function<void ()> Job;
    
    static void empty_thread_initializer() { ; }
    
    IPCThreadPool( size_t size, const Job & thread_initializer = empty_thread_initializer );
    
    ~IPCThreadPool();
        
    void addJob( const Job & job );
    
    void cancel( );
        
    void waitForCompletion() const;
    
    size_t size() const 
    { return m_workers.size(); }
    
  private:
            
    struct Worker : boost::mutex
    {
    	Worker( )
          : m_stop( false )
        { ; }
        
        bool awake_condition( const std::queue<Job> * jobs )
        { return ( m_stop || !jobs -> empty() ); }
        
        void stop() 
        { m_stop = true; }
        
        bool stopped() 
        { return m_stop; }
        
      private:    
        bool	m_stop;
    };

    typedef boost::shared_ptr<Worker> WorkerPtr;
        
  private:    
    void run( const Job & thread_initializer, Worker * worker );
    void stopAllThreads();
    void waitAllJobsDone() const;
    
    void defaultJobSubmitter(const Job &);
    void nullJobSubmitter(const Job & job) { job(); }
    
  private:
    mutable boost::mutex		m_mutex;
    mutable boost::condition		m_all_jobs_done;
    boost::condition			m_jobs_available;
    boost::thread_group			m_pool;
    std::vector<WorkerPtr>		m_workers;
    std::queue<Job>			m_jobs;
    boost::function<void (const Job &)> m_job_submitter;
};

typedef IPCThreadPool IPCPipeline;

#endif
