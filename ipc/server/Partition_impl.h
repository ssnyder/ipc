#ifndef IPC_PARTITION_IMPL_H
#define IPC_PARTITION_IMPL_H

#include <ers/ers.h>

#include <ipc/alarm.h>
#include <ipc/namedobject.h>
#include <ipc/signal.h>
#include <ipc/server/CosNaming_impl.h>

class Partition_impl :	public IPCNamedObject<POA_ipc::partition>,
			public CosNaming::NamingContext_impl
{
  public:
    Partition_impl(	const std::string & backup_dir,
                        unsigned long checkpoint_period,
                        unsigned long busy_threshold,
                        bool do_restore,
                        bool do_backup );
                        
    Partition_impl(	const std::string & name, 
                        const std::string & backup_dir,
                        unsigned long checkpoint_period,
                        unsigned long busy_threshold,
                        bool do_restore,
                        bool do_backup );
                        
    ~Partition_impl();
    
    void publish() {
    	partition().publish( name(), std::string(), _this() );
        m_published = true;
    }
    
    void withdraw() {
    	partition().withdraw( name(), std::string() );
        m_published = false;
    }

    void withdraw( const std::string & name ) {
    	partition().withdraw( name, std::string() );
    }

    void publish( const std::string & name ) {
    	IPCPartition( ).publish( name, std::string(), _this() );
    }
    
    void publish( const std::string & pname, const std::string & name ) {
    	IPCPartition( pname ).publish( name, std::string(), _this() );
    }
    
    void shutdown() {
	ERS_DEBUG( 1, "shutdown request received" );
    	daq::ipc::signal::raise();
    }
    
  private:
    static bool alarm_function( void * rock );
    
    void doBackup();
    void doRestore();
    
    bool			m_do_backup;
    std::string			m_backup_directory;
    IPCAlarm *			m_alarm;
    std::string			m_backup_file_name;
    bool			m_published;
    unsigned int		m_checkpoint_period;
    unsigned int		m_busy_threshold;
};

#endif
