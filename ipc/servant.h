#ifndef IPC_SERVANT_H
#define IPC_SERVANT_H

////////////////////////////////////////////////////////////////////////////
//    servant.h
//
//    header file for the IPC library
//
//    Sergei Kolos June 2003
//
//    description:
//    	IPCEntityServant - implements ipc::servant interface
//
////////////////////////////////////////////////////////////////////////////

#include <omniORB4/CORBA.h>

#include <ipc/ipc.hh>

class IPCPartition;

class IPCServant : public virtual POA_ipc::servant
{
  public:      
    virtual ~IPCServant() { ; }
    
    void ping();

    void shutdown();

    void test(const char *);
    
    CORBA::Short debug_level();

    void debug_level( CORBA::Short new_level );

    CORBA::Short verbosity_level();

    void verbosity_level( CORBA::Short new_level );

    ipc::servant::ApplicationContext * app_context();
    
    ipc::servant::PartitionContext * partition_context();

  protected:
    IPCServant(	const IPCPartition & partition = IPCPartition(), 
    		const std::string & name = "");
        
  private:
    ipc::servant::ApplicationContext_var	m_app_context;
    ipc::servant::PartitionContext_var		m_partition_context;
};

#endif
