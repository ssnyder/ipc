////////////////////////////////////////////////////////////////////////
//    ipc_register_object.cc
//
//    Sergei Kolos,    January 2000
//
//    description:
//      Register IPC object reference with the given IPC partition
//
////////////////////////////////////////////////////////////////////////

#include <iostream>

#include <ers/ers.h>

#include <cmdl/cmdargs.h>

#include <ipc/partition.h>
#include <ipc/core.h>
#include <ipc/util.h>

int main(int argc, char ** argv)
{    
    CmdArgStr	partition_name	( 'p', "partition", "partition-name", 
    					"name of the partition where object has to be registered." );
    CmdArgStr	reference	( 'r', "reference", "object-reference", 
    					"object reference to be registered.", CmdArg::isREQ );
    CmdArgStr	type		( 't', "type", "object-type", 
    					"type of the object to be registered "
					"(by default the object will be registered as ipc/partition)." );
    CmdArgStr	name		( 'n', "name", "object-name", 
    					"name of the object to be registered.", CmdArg::isREQ );
    CmdArgBool	verify		( 'v', "verify", 
    					"Test existence of the given object." );

    // Initialise IPC
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }
    
    // Declare command object and its argument-iterator
    CmdLine	cmd(*argv, &partition_name, &reference, &type, &name, &verify, NULL);
    CmdArgvIter	arg_iter(--argc, ++argv);
    
    cmd.description( "Register IPC object reference with the given IPC partition." );
    
    type = "";
    cmd.parse(arg_iter);
    
    IPCPartition partition( partition_name );
    CORBA::Object_var object;
    try {
	object = IPCCore::stringToObject( (const char*)reference );
        ipc::util::bind( partition.name(), (const char*)name, (const char*)type, object );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 2;
    }
    
    if ( verify.flags() && CmdArg::GIVEN )
    {
        bool exists = false;
        try {
	    ipc::partition_var partition = ipc::partition::_narrow( object );

	    exists = !object -> _non_existent();
	    std::cout << "Remote object " << ( exists ? "exists" : "does not exist" ) << std::endl;
	}
	catch( daq::ipc::Exception & ex ) {
	    std::cout << "Remote object does not exist (" << ex << ")" << std::endl;
	}
	catch ( CORBA::SystemException & ex ) {
	    std::cout << "Remote object does not exist (" << ex << ")" << std::endl;
	}	
    }
        
    return 0;
}
