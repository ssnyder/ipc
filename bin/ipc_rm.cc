////////////////////////////////////////////////////////////////////////
//    ipc_rm.cc
//
//    IPC remove main function
//
//    Sergei Kolos,    January 2000
//
//    description:
//      Shutdown services registered in the IPC naming service
//
////////////////////////////////////////////////////////////////////////

#include <iostream>

#include <boost/regex.hpp>

#include <ers/ers.h>

#include <cmdl/cmdargs.h>

#include <ipc/partition.h>
#include <ipc/core.h>

long ServersDestroyed = 0;

bool killServer( ipc::servant_ptr servant,
		 const std::string & type_name,
                 const std::string & name,
                 bool trace )
{    
    if ( trace )
	std::cout << ">> destroying the \"" << name << "\" server ... ";
    
    if ( CORBA::is_nil(servant) )
    {
	if ( trace )
            std::cout << "error" << std::endl;

	std::cerr << "ERROR [ipc_rm]: Can't destroy the \"" 
        	<< type_name << "\" object \"" << name << "\"." << std::endl;
	std::cerr << "Reason: Either it does not exist or does not implement the ipc::servant interface" << std::endl;
        return false;
    }
    
    try {    
        servant -> shutdown();
    }
    catch(CORBA::NO_PERMISSION &) {
	if ( trace )
            std::cout << "error" << std::endl;

        std::cerr << "ERROR [ipc_rm]: Can't destroy the \"" 
        	<< type_name << "\" object \"" << name << "\"." << std::endl;
        std::cerr << "Reason: You are not the owner." << std::endl;
        return false;
    }
    catch(CORBA::SystemException & e) {
	if ( trace )
            std::cout << "error" << std::endl;

        std::cerr << "ERROR [ipc_rm]: Can't destroy the \"" 
        	<< type_name << "\" object \"" << name << "\"." << std::endl;
        std::cerr << "Reason: It seems it does not exist " << std::endl;
        return true;
    }
    
    if ( trace )
        std::cout << "ok" << std::endl;
    
    ServersDestroyed++;
    return true;    
}

bool destroyServers(	const IPCPartition partition, 
			const std::string & type_name, 
			const boost::regex & object_criteria, 
			bool trace )
{
    bool status = true;
    
    std::map< std::string, ipc::servant_var > objects;
    
    partition.getObjects<ipc::servant,ipc::use_cache,ipc::unchecked_narrow>( objects, type_name );
    
    std::map< std::string, ipc::servant_var >::iterator it = objects.begin();
    
    while( it != objects.end() )
    {
        if ( boost::regex_match( (*it).first, object_criteria ) )
        {
            status &= killServer( (*it).second, type_name, (*it).first, trace );
        }
	it++;
    }
    return status;
}

bool destroyContexts(	const IPCPartition p, 
			const boost::regex & object_criteria, 
			const boost::regex & type_criteria, 
			bool trace )
{
    bool status = true;
            
    std::list< std::string > t_names;
    
    p.getTypes( t_names );
    
    for ( std::list< std::string >::iterator it = t_names.begin(); it != t_names.end(); it++ )
    {    
        if ( boost::regex_match( *it, type_criteria ) )
        {
            if ( trace )
                std::cout << ">> destroying servers of the \"" << *it << "\" type ... " << std::endl;

            status &= destroyServers( p, *it, object_criteria, trace );
        }
	
	if ( trace )
	    std::cout << ">> done" << std::endl;
    }
    return status;
}

bool destroyPartition(	const IPCPartition partition,
			const boost::regex & object_criteria,
			const boost::regex & type_criteria,
			bool force,
			bool trace )
{

    if ( trace )
	std::cout << ">> destroying \"" << partition.name() << "\" partition ..." << std::endl;
    
    bool all_servers_destroyed = destroyContexts( partition, object_criteria, type_criteria, trace );
    
    if ( force )
    {
	if ( !all_servers_destroyed )
	{
	    std::cerr << "ERROR [ipc_rm]: Can't destroy the \"" 
            	<< partition.name() << "\" partition " << std::endl;
	    std::cerr << "Reason: There are still some servers registered with the \"" 
            	<< partition.name() << "\" partition." << std::endl;
	    return all_servers_destroyed;
        }
        	
	try {
	    partition.getImplementation()->shutdown();
	}
	catch(CORBA::NO_PERMISSION &) {
            std::cerr << "ERROR [ipc_rm]: Can't destroy the \"" 
            	<< partition.name() << "\" partition " << std::endl;
	    std::cerr << "Reason: You are not the owner." << std::endl;
	    all_servers_destroyed = false;
        }
	catch(...) {
            std::cerr << "ERROR [ipc_rm]: Can't destroy the \"" 
            	<< partition.name() << "\" partition " << std::endl;
            std::cerr << "Reason: It seems it does not exist" << std::endl;
	}
	
	if ( trace )
	    std::cout << ">> done" << std::endl;
    }
    return all_servers_destroyed;
}

bool destroyRootPartition(	const boost::regex & object_criteria, 
				const boost::regex & type_criteria, 
				bool force, 
				bool trace )
{
    bool all_partitions_destroyed = true;
        
    if ( force )
    {        
	std::list < IPCPartition> pl;
    
	IPCPartition::getPartitions( pl );
	 
	for ( std::list < IPCPartition>::iterator it = pl.begin(); it != pl.end(); it++ )
	{
            try {
                all_partitions_destroyed &= destroyPartition( 
                	*it, object_criteria, type_criteria, force, trace );
            }
            catch( daq::ipc::InvalidPartition & ex ) {
		std::cerr   << "ERROR [ipc_rm]: Can't destroy the \"" << it->name() << "\" partition\n"
			    << "Reason: It seems it does not exist" << std::endl;
            }
	}
    }        
    
    if (all_partitions_destroyed)
    {
	IPCPartition partition;
	try {
	    all_partitions_destroyed &= destroyPartition( 
            	partition, object_criteria, type_criteria, force && all_partitions_destroyed, trace );
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    std::cerr	<< "ERROR [ipc_rm]: Can't destroy the \"" << partition.name() << "\" partition\n"
			<< "Reason: It seems it does not exist" << std::endl;
	}
    }
        
    return all_partitions_destroyed;
}

int main(int argc, char ** argv)
{
    try {
	std::list< std::pair< std::string, std::string > > opt = IPCCore::extractOptions( argc, argv );
	opt.push_front( std::make_pair( std::string("scanGranularity"), std::string("1") ) );
	opt.push_front( std::make_pair( std::string("inConScanPeriod"), std::string("2") ) );
        IPCCore::init( opt );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }
    
      // Declare arguments
    CmdArgBool trace		('t', "trace", "Trace execution.");
    CmdArgBool force		('f', "force",        
					"Destroy a partition itself.");
    CmdArgStr  server_name	('n', "name", "server-name",
					"A name of server to be destroyed.\n"
					"Regular expresssions allowed.\n"
					"[4mNOTE[0m: [1m\".*\"[0m matches any name",
					CmdArg::isREQ);
    CmdArgStr  partition_name	('p', "partition", "partition-name", "Destroy applications for this partition.");
    CmdArgBool ref		('R', "reference", "Print the IPC initial reference.");
    CmdArgStr  type_name	('i', "interface", "interface-name", 
					"A name of interface to be destroyed.\n"
					"Regular expressions allowed.\n"
					"Valid interface names for example are: \n"
					"   mrs/router, is/repository, monitoring/sampler, rdb/cursor"
					"   rc/controller, dsa/supervisor, pmg/AGENT, etc.\n"
					"[4mNOTE[0m: [1m\".*\"[0m matches any name",
					CmdArg::isREQ);
    
       // Declare command object and its argument-iterator
    CmdLine  cmd(*argv, &trace, &force, &partition_name, &type_name, &server_name, &ref, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

    cmd.description( "Shutdown applications for the given partition.\n" );
  
    // Parse arguments
    cmd.parse(arg_iter);
   
    //
    // Print location of the IPC reference file
    //
    
    if ( ref.flags() && CmdArg::GIVEN )
    {
	std::cout << "Initial reference is " << ipc::util::getInitialReference() << std::endl;
    }
    
    boost::regex object_criteria;
    try {
	object_criteria = boost::regex( server_name );
    }
    catch ( boost::bad_expression & ex ){
	std::cerr << "ERROR [ipc_ers]: Invalid regular expression '" 
        	<< server_name << "'" << std::endl;
	return 2;
    }

    boost::regex type_criteria;
    try {
	type_criteria = boost::regex( type_name );
    }
    catch ( boost::bad_expression & ex ){
	std::cerr << "ERROR [ipc_ers]: Invalid regular expression '" 
        	<< type_name << "'" << std::endl;
	return 2;
    }
      
    bool status = false;
   
    if ( partition_name.flags() & CmdArg::GIVEN )
    {
	IPCPartition  partition( (const char*)partition_name );
	try {
	    status = destroyPartition( partition, object_criteria, type_criteria, force, trace );
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    std::cerr	<< "ERROR [ipc_rm]: Can't destroy the \"" << partition.name() << "\" partition\n"
			<< "Reason: It seems it does not exist" << std::endl;
	}
    }
    else
    {
	status = destroyRootPartition( object_criteria, type_criteria, force, trace );
    }
   
    if ( ServersDestroyed == 0 && status )
    {
	std::cerr << "\"" << type_name << "\"::" << "\"" << server_name << "\" : No such servers" << std::endl;
    }
    return 0;
}
