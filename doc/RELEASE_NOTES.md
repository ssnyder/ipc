
# IPC

### TLS Support

IPC based servers and clients can use TLS
to protect their communication.

The `TDAQ_IPC_ENABLE_TLS` environment variable
can be used to turn this capability on
(if value = "1") or off (if value = "0"). This
is typically a global setting as part of the
common environment. If enabled clients will
be able to talk to servers which require TLS.

For a C++ based server to enforce TLS two
environment variables should be set in its
environment (in addition to the variable
above):

    ORBendPoint=giop:ssl::
    ORBendPointPublish=giop:ssl::

An OKS `VariableSet` called `TDAQ_IPC_TLSSERVER_Environment`
is available that can be linked to an
`Application` or `Binary` object.

For testing the variables can be also set
manually in an interactive environment.

For Java based servers the following
property has to be set:

    jacorb.security.ssl.server.required_options=20

There is an overhead involved in setting up an
TLS connection, as well as for the
encryption during the data transfer phase. Therefore
it should be used only for servers that receive
confidential data like authorization tokens, and it
should be avoided for anything where potentially
large amounts of data are exchanged, like IS servers.

In practice currently only the following applications
require this:

  * pmgserver
  * run controllers
  * master trigger implementations
  * RDB writer

Note that the way TLS is used in the IPC package does not provide authentication,
only confidentiality and integrity. Authentication has to be handled on the application level.
