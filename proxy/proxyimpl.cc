////////////////////////////////////////////////////////////////////////////
//      proxy.cc
//
//      Implementation of the IPC proxy library
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains implementation of the IPC service proxy
//
////////////////////////////////////////////////////////////////////////////

#include <partition/partition.hh>
#include <ipc/proxy/factory.h>

namespace
{
  IPCProxyFactory<CosNaming::NamingContext, POA_CosNaming::NamingContext, POA_CosNaming::NamingContext_tie> __f1__;
  IPCProxyFactory<CosNaming::BindingIterator, POA_CosNaming::BindingIterator, POA_CosNaming::BindingIterator_tie> __f2__;
  IPCProxyFactory<ipc::NamingContextOpt, POA_ipc::NamingContextOpt, POA_ipc::NamingContextOpt_tie> __f3__;
  IPCProxyFactory<ipc::partition, POA_ipc::partition, POA_ipc::partition_tie> __f4__;
}
