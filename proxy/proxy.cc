////////////////////////////////////////////////////////////////////////////
//      proxyimpl.cc
//
//      Implementation of the IPC proxy application
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains implementation of the IPC proxy
//
////////////////////////////////////////////////////////////////////////////

#include <ipc/proxy/proxy.h>
#include <ipc/proxy/manager.h>

bool
IPCProxy::exists()
{
    try {
	std::string reference = ipc::util::constructReference(	    ipc::proxy::name,
								    ipc::util::getTypeName<ipc::proxy>(),
								    IPC_PROXY_CONNECT_FILE_NAME );

	CORBA::Object_var object = IPCCore::stringToObject( reference );
        
	ipc::proxy_var proxy = ipc::proxy::_narrow( object );

	if ( CORBA::is_nil( proxy ) )
	{
	    ERS_DEBUG( 1, "Can not connect to the IPC proxy application" );
            return false;
	}
        
        proxy -> ping();
    }
    catch( daq::ipc::Exception & ex ) {
	ERS_DEBUG( 1, "Got '" << ex << "' exception" );
        return false;
    }
    catch( CORBA::SystemException & ex ) {
	ERS_DEBUG( 1, "Got '" << ex._name() << "' CORBA exception" );
        return false;
    }
    return true;
}

CORBA::Object_ptr 
IPCProxy::create( const char * interface, const char * ior )
{
    CORBA::Object_ptr object = CORBA::Object::_nil();
    try {
    	object = IPCSingleton<IPCProxyManager>::instance().createProxy( interface, ior );
    }
    catch( CORBA::INV_OBJREF & ex ) {
	ERS_DEBUG( 1, "Got invalid object reference exception " << ex );
    	throw ipc::proxy::BadReference( interface, ior );
    }
    catch( CORBA::SystemException & ex ) {
	ERS_DEBUG( 1, "Got system exception " << ex );
    	throw ipc::proxy::TargetDoesNotExist( ior );
    }
    
    if ( CORBA::is_nil( object ) )
    {
	throw ipc::proxy::InterfaceNotSupported( interface );
    }
    return object;
}

void
IPCProxy::get_supported_interfaces( ipc::proxy::list_out interfaces )
{
    std::list<std::string> list;
    IPCSingleton<IPCProxyManager>::instance().getRegisteredInterfaces( list );

    size_t idx = 0;
    interfaces = new ipc::proxy::list( list.size() );
    interfaces->length( list.size() );
    for ( std::list<std::string>::iterator it = list.begin(); it != list.end(); ++it )
    {
	interfaces[idx++] = (*it).c_str();
    }
}

std::string
IPCProxy::unique_id() const
{
    return ipc::proxy::name;
}

