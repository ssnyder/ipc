////////////////////////////////////////////////////////////////////////////
//      manager.cc
//
//      Implementation of the IPC proxy application
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains implementation of the IPC proxy manager
//
////////////////////////////////////////////////////////////////////////////

#include <ipc/core.h>
#include <ipc/pluginfactory.h>
#include <ipc/singleton.h>
#include <ipc/proxy/manager.h>

namespace
{
  const char * const environment_name = "TDAQ_IPC_PROXY_PLUGINS";
  const char * const default_value = "ipcproxy";
}

IPCProxyManager::~IPCProxyManager( )
{
    boost::mutex::scoped_lock lock( mutex_ );
    for ( ProxyObjects::iterator it = objects_.begin( ); it != objects_.end(); )
    {
	ERS_DEBUG( 1, "removing the '" << it->first << "' remote object" );
	it->second -> __destroy__();
	objects_.erase( it++ );
    }
    
    for ( ProxyFactories::iterator it = factories_.begin( ); it != factories_.end(); )
    {
	ERS_DEBUG( 1, "removing the '" << it->first << "' remote object" );
	factories_.erase( it++ );
    }
}

void 
IPCProxyManager::init( const std::string & args )
{
    std::string plugins = args + ':';
    std::string::size_type start = 0, end;
    while( ( end = plugins.find( ':', start ) ) != std::string::npos )
    {
	std::string plugin = plugins.substr( start, end - start );
	start = end + 1;

	if ( plugin.empty() )
	    continue;
            
	try {
	    ERS_DEBUG( 1, "loading the '" << plugin << "' plugin" );
	    IPCSingleton< IPCPluginFactory<IPCProxyFactoryBase> >::instance().load( plugin );
	    ERS_DEBUG( 1, "the '" << plugin << "' plugin was successfully loaded" );
	}
	catch( daq::ipc::PluginException & ex ) {
	    ers::error( ex );
	}
	catch( daq::ipc::IgnorePluginException & ex ) {
	    ers::debug( ex, 1 );
	}
	catch(...) {
	    ers::error( daq::ipc::PluginException( ERS_HERE, plugin, "unknown" ) );
	}
    }
}

void IPCProxyManager::init(  )
{
    const char * var = getenv( environment_name );
    if ( !var )
    {
	var = default_value;
    }
    init( var );
}

void
IPCProxyManager::getRegisteredInterfaces( std::list<std::string> & list )
{
    boost::mutex::scoped_lock lock( mutex_ );
    ProxyFactories::iterator it = factories_.begin( );
    for ( ; it != factories_.end(); ++it )
    {
    	list.push_back( it->first );
    }
}

CORBA::Object_ptr
IPCProxyManager::createProxy( const std::string & interface, const std::string & ior )
{
    ERS_DEBUG( 1, "request to create proxy for the '" << ior << "' object of the '"
            << interface << "' interface" );

    boost::mutex::scoped_lock lock( mutex_ );

    ProxyObjects::iterator ito = objects_.find( ior );
    if ( ito != objects_.end() )
    {
	ERS_DEBUG( 1, "proxy object is found in the cache" );
	return CORBA::Object::_duplicate( ito->second->__reference__() );
    }
    
    ERS_DEBUG( 1, "looking for the factory of the '" << interface << "' interface" );
    ProxyFactories::iterator itf = factories_.find( interface );
    if ( itf != factories_.end() )
    {
	ERS_DEBUG( 1, "factory of the '" << interface << "' interface has been found" );
	IPCProxyObjectBase * proxy = itf->second->create( ior );
	
        CORBA::Object_ptr ref = proxy->__reference__();
        ERS_LOG( "New proxy object '" << IPCCore::objectToString(ref, IPCCore::Corbaloc)
                << "' has been created for the target '" << ior << "' object" );

        objects_.insert( std::make_pair( ior, proxy ) );
        return CORBA::Object::_duplicate( ref );
    }
    
    ERS_DEBUG( 1, "interface '" << interface << "' is not supported by the proxy" );
    return CORBA::Object::_nil();
}

CORBA::Object_ptr
IPCProxyManager::createProxy( const IPCPartition & partition, const std::string & name,
        const std::string & interface, CORBA::Object_ptr remote )
{
    std::string ior = IPCCore::objectToString(remote, IPCCore::Corbaloc);
    ERS_DEBUG( 1, "request to create proxy for the '" << ior << "' object of the '"
            << interface << "' interface" );

    boost::mutex::scoped_lock lock( mutex_ );

    ProxyObjects::iterator ito = objects_.find( ior );
    if ( ito != objects_.end() )
    {
        ERS_DEBUG( 1, "proxy object is found in the cache" );
        return CORBA::Object::_duplicate( ito->second->__reference__() );
    }

    ERS_DEBUG( 1, "looking for the factory of the '" << interface << "' interface" );
    ProxyFactories::iterator itf = factories_.find( interface );
    if ( itf != factories_.end() )
    {
        ERS_DEBUG( 1, "factory of the '" << interface << "' interface has been found" );
        IPCProxyObjectBase * proxy = itf->second->create( partition, name, remote );
        if (proxy == nullptr) {
            return CORBA::Object::_nil();
        }

        CORBA::Object_ptr ref = proxy->__reference__();
        ERS_DEBUG( 1, "New proxy object '" << IPCCore::objectToString(ref, IPCCore::Corbaloc)
                << "' has been created for the target '" << ior << "' object" );

        objects_.insert( std::make_pair( ior, proxy ) );
        return CORBA::Object::_duplicate( ref );
    }

    ERS_DEBUG( 1, "interface '" << interface << "' is not supported by the proxy" );
    return CORBA::Object::_nil();
}

void 
IPCProxyManager::cleanup()
{
    boost::mutex::scoped_lock lock( mutex_ );

    for ( ProxyObjects::iterator it = objects_.begin( ); it != objects_.end(); )
    {
	ERS_DEBUG( 1, "checking existence of the '" << it->first << "' remote object" );
	if ( !it->second->__target_exists__() )
        {
	    ERS_DEBUG( 1, "the '" << it->first
	            << "' remote object does not exist, removing its proxy object" );
            it->second -> __destroy__();
	    ERS_LOG( "Proxy object " << it->first << " has been destroyed" );
            
            objects_.erase( it++ );
        }
        else
        {
	    ERS_DEBUG( 1, "the '" << it->first << "' remote object exists" );
            ++it;
        }
    }
}

void 
IPCProxyManager::destroy()
{
    delete this;
}

