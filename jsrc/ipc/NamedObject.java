package ipc;

/**
 * 
 * @author Serguei Kolos
 * 
 *        This class is designed for simplifying implementation of interfaces
 *        declared in CORBA IDL. User has to inherit an IDL interface
 *        implementation class from this one in order to have it implicitly
 *        linked with a suitable Portable Object Adapter(POA). This way a new
 *        instance of a user class will be automatically activated with this
 *        POA at appropriate time. In addition to that this class provides
 *        functions for publishing the object reference to the IPC Naming
 *        Service and for removing this publication.
 *        <p>
 *        Note that this class should be used for implementing IDL interfaces,
 *        which inherit the <tt>ipc::servant</tt> base interface. The <tt>ipc::servant</tt>  
 *        interface defines few common operations for remote objects: pinging, shutting down
 *        and retrieving context of the application where the object resides. This class
 * 	   provides default implementation for those operations, while a user specific 
 * 	   class may override them is required.
 * 
 * @param <Type>
 *           type of the CORBA interface, which is implemented by this
 *           object. A valid type is a Java <tt>abstract interface</tt> which
 *           is generated from the respective IDL <tt>interface</tt>
 *           declaration. For example to implement the <tt>is::repository</tt>
 *           IDL interface
 *           <pre>
 *           {@code 
 *           	#include <ipc/ipc.idl>
 *           	module is {
 *           		interface repository : ipc::servant { ... } 
 *           	}; 
 *           }
 *           </pre>
 *           one should declare a new Java class inheriting from the
 *           ipc.NamedObject and implementing functions declared in the
 *           <tt>is::repository</tt> interface: 
 *           <pre>
 *           {@code 
 *           	class Repository extends NamedObject<is.repository> {
 *           	  Repository(Partition partition, String name) 
 *           	  { 
 *           	    this(partition, name);
 *           	  } 
 *           	} 
 *           } 
 *           </pre>
 *           An instance of the Repository class can be published to the IPC Naming Service as
 *           shown in the following code fragment:
 *           <pre>
 *           {@code
 * 		ipc.Partition p = new Partition("ATLAS");
 * 		Repository rep = new Repository(p, "RunParams");
 * 		rep.publish();
 * 	      }
 *           </pre>
 * 
 */
public class NamedObject<Type extends ipc.servant> extends Object<Type>
{
    public final Partition partition;
    public final String name;

    /**
     * Creates new NamedObject which implements CORBA interface defined by the
     * <tt>Type</tt> parameter.
     * 
     * @param partition
     *            represents the IPC communication domain in the Naming Service
     * @param name
     *            This name will be bound to the object reference in the IPC
     *            Naming Service. Other applications can use this name to obtain
     *            the object reference and use it for communicating with this
     *            object.
     */
    protected NamedObject(Partition partition, String name)
    {
	super(partition.getName(), name);
	this.partition = partition;
	this.name = name;
    }

    /**
     * Checks if the object reference is already published to the IPC Naming
     * Service.
     * 
     * @return true is the object reference is already known to the Naming
     *         Service, false otherwise.
     * @throws InvalidPartitionException
     */
    public boolean isPublished() throws InvalidPartitionException
    {
	if (reference == null) {
	    return false;
	}

	try {
	    org.omg.CORBA.Object object = NamingService.resolve(partition.getName(), type, name);
	    return object._is_equivalent(reference);
	}
	catch (InvalidObjectException e) {
	    return false;
	}
    }

    /**
     * Bind the object reference to the name given at construction and send this
     * binding to the IPC Naming Service.
     * 
     * @throws InvalidPartitionException
     *             if the IPC Naming Service domain represented by the partition
     *             object, used at the object construction, does not exist.
     */
    public void publish() throws InvalidPartitionException
    {
	NamingService.bind(partition.getName(), type, name, _this());
    }

    /**
     * Remove the object reference from the IPC Naming Service.
     * 
     * @throws InvalidPartitionException
     *             if the IPC Naming Service domain represented by the partition
     *             object, used at the object construction, does not exist.
     */
    public void withdraw() throws InvalidPartitionException
    {
	NamingService.unbind(partition.getName(), type, name);
    }

    public void shutdown()
    {
	java.lang.System.exit(0);
    }
}
