package ipc;

/**
 * Thrown to indicate that requested partition does not exist or is not
 * reachable via network.
 * 
 * @author Serguei Kolos
 * @since 20/01/01
 */
public class InvalidPartitionException extends ers.Issue
{
    /**
     * Constructs an exception object with the specified partition.
     * 
     * @param name	partition name
     */
    public InvalidPartitionException(String name, Exception cause)
    {
	super("Partition '" + name + "' does not exist", cause);
    }
}
