package ipc;

import ipc.Cache.Item;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.omg.CORBA.Any;
import org.omg.CORBA.ORB;
import org.omg.CORBA.TCKind;
import org.omg.IOP.ServiceContext;
import org.omg.PortableInterceptor.ClientRequestInterceptor;
import org.omg.PortableInterceptor.ClientRequestInfo;
import org.omg.PortableInterceptor.Current;
import org.omg.PortableInterceptor.CurrentHelper;
import org.omg.PortableInterceptor.InvalidSlot;
import org.omg.PortableInterceptor.ORBInitInfo;
import org.omg.PortableInterceptor.ForwardRequest;

class ClientInterceptor extends org.omg.CORBA.LocalObject implements
	ClientRequestInterceptor
{
    private static final int SECURE_SHUTDOWN_ID = 0x1001;
    
    private final Current current;
    private final int slot;

    public ClientInterceptor(ORBInitInfo info) throws Exception {
    	this.current = CurrentHelper.narrow(info.resolve_initial_references("PICurrent"));
        this.slot = info.allocate_slot_id();
    }
    
    public String name()
    {
	return "ipc.ClientInterceptor";
    }

    public void destroy()
    {
    }

    public void send_request(ClientRequestInfo ri)
    {
	org.omg.CORBA.Object object = ri.target();
	ers.Logger.debug(2, "Invoking '" + ri.operation() 
		+ "' on " + Core.objectToString(object, Core.CORBALOC) + " object");

	try {
	    Any any = ORB.init().create_any();
	    any.insert_string(Core.objectToString(object, Core.IOR));
	    this.current.set_slot(this.slot, any);
	} catch (InvalidSlot ex) {
	    ers.Logger.debug(1, ex);
	}
	
	if (ri.operation().equals("shutdown")) {
	    byte rid[] = System.getProperty("user.name").getBytes();
	    if (rid.length > 0) {
		byte bytes[] = new byte[rid.length + 1];
		for (int i = 0; i < rid.length; i++)
		    bytes[i] = rid[i];
		bytes[bytes.length - 1] = 0;

		ri.add_request_service_context(new org.omg.IOP.ServiceContext(
			SECURE_SHUTDOWN_ID, bytes), false);
	    }
	}
    }

    public void send_poll(ClientRequestInfo ri)
    {
    }

    public void receive_reply(ClientRequestInfo ri)
    {
    }

    public void receive_other(ClientRequestInfo ri)
    {
    }

    public void receive_exception(ClientRequestInfo ri) throws ForwardRequest
    {
	String threadName = Thread.currentThread().getName();

	org.omg.CORBA.SystemException exception;
	try {
	    exception = org.omg.CORBA.SystemExceptionHelper.extract (ri.received_exception());
	}
	catch (Exception e) {
	    ers.Logger.debug(1, threadName + ": Received non system exception.");
	    return ;
	}
	
	if (exception.completed == org.omg.CORBA.CompletionStatus.COMPLETED_YES) {
	    ers.Logger.debug(1, threadName + ": Completion status is 'YES', no recovery attempt will be made");
	    return ;
	}
	
	ers.Logger.debug(1, threadName + ": [" + exception + "] exception received");
		    
	org.omg.CORBA.Object object = ri.target();
        String currentTarget = Core.objectToString(object, Core.IOR);
	ers.Logger.debug(1, threadName + ": target = " + currentTarget);
	
        String originalTarget;
        org.omg.CORBA.Object new_object; 
	try {
	    Any any = ri.get_slot(this.slot);
	    originalTarget = any.extract_string();
	    ers.Logger.debug(1, threadName + ": original target = " + originalTarget);
	    
	    new_object = Cache.recover(object);
	} catch (Exception ex) {
	    ers.Logger.debug(1, ex);
	    return;
	}

	String newTarget = Core.objectToString(new_object, Core.IOR);
        ers.Logger.debug(1, threadName + ": new target = " + newTarget);
        
	if (originalTarget.equals(newTarget)) {
	    ers.Logger.debug(1, threadName
	            + ": The last known object reference is the same as the failed one,"
	            + " no further recovery attempts will be made.");
	    return;
	}

	if (newTarget.equals(currentTarget)) {
	    ers.Logger.debug(1, threadName + ": Recovery had been done in another thread, invoke the same request once more.");
	    throw new ForwardRequest();
	} else {
	    ers.Logger.debug(1, threadName + ": Invoke the same request with the new object reference.");
	    throw new ForwardRequest(new_object);
	}
    }
}
