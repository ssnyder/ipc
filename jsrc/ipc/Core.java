package ipc;

import java.util.Properties;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.IOP.IOR;
import org.omg.IOP.TaggedProfile;
import org.omg.IOP.TAG_INTERNET_IOP;

import org.jacorb.orb.iiop.IIOPProfile;
import org.jacorb.orb.iiop.IIOPAddress;
import org.jacorb.orb.ParsedIOR;

public abstract class Core
{
    private static final ORB orb;
    private static final POA poa;

    public static final int IOR = 1;
    public static final int CORBALOC = 2;

    private static final char[] HEXCHAR = { 
	'0', '1', '2', '3', '4', '5', '6', '7', 
	'8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

    static {
	String timeout = java.lang.System.getenv("TDAQ_IPC_TIMEOUT") != null ? java.lang.System
		.getenv("TDAQ_IPC_TIMEOUT") : "10000";
	Properties props = new Properties();
	props.put("org.omg.PortableInterceptor.ORBInitializerClass."
		+ "ipc.ORBInitializer", "");
	props.put("org.omg.CORBA.ORBClass", "org.jacorb.orb.ORB");
	props.put("org.omg.CORBA.ORBSingletonClass",
		"org.jacorb.orb.ORBSingleton");
	props.put("jacorb.implname", "");
	props.put("jacorb.retries", System.getProperty("jacorb.retries", "0"));
	props.put("jacorb.poa.thread_pool_min",
		System.getProperty("jacorb.poa.thread_pool_min", "25"));
	props.put("jacorb.poa.thread_pool_max",
		System.getProperty("jacorb.poa.thread_pool_max", "150"));
	props.put("jacorb.poa.queue_min",
		System.getProperty("jacorb.poa.queue_min", "49900"));
	props.put("jacorb.poa.queue_max",
		System.getProperty("jacorb.poa.queue_max", "50000"));
	props.put("jacorb.poa.queue_wait",
		System.getProperty("jacorb.poa.queue_wait", "on"));
	props.put("jacorb.connection.client.connect_timeout",
		System.getProperty("tdaq.ipc.timeout", timeout));
	props.put("jacorb.connection.client.pending_reply_timeout",
		System.getProperty("tdaq.ipc.timeout", timeout));

    String ssl = java.lang.System.getenv("TDAQ_IPC_ENABLE_TLS");
    if(ssl != null && ssl.equals("1")) {
        props.put("jacorb.security.support_ssl", "on");
        // Custom trust manager for self signed certs
        props.put("jacorb.security.ssl.client.trust_manager",
                  System.getProperty("jacorb.security.ssl.client.trust_manager",
                                     "ipc.TrustManager"));
        props.put("jacorb.ssl.socket_factory",
                  System.getProperty("jacorb.ssl.socket_factory",
                                     "org.jacorb.security.ssl.sun_jsse.SSLSocketFactory"));
        props.put("jacorb.ssl.server_socket_factory",
                  System.getProperty("jacorb.ssl.server_socket_factory",
                                     "ipc.SSLServerSocketFactory"));

        props.put("jacorb.security.ssl.client.required_options",
                  System.getProperty("jacorb.security.ssl.client.required_options" ,"0"));
        props.put("jacorb.security.ssl.server.required_options",
                  System.getProperty("jacorb.security.ssl.server.required_options", "0"));

        // We need these two entries to exist for the SSLSocketFactory which
        // reads them at configuration (but does not use them in our setup)
        props.put("jacorb.security.keystore",
                  System.getProperty("jacorb.security.keystore", ""));
        props.put("jacorb.security.keystore_password",
                  System.getProperty("jacorb.security.keystore_password", ""));
    }

	try {
	    orb = ORB.init((String[]) null, props);
	    poa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
	    poa.the_POAManager().activate();
	}
	catch(Exception e) {
	    throw new ExceptionInInitializerError(e);
	}
    }

    public static org.omg.CORBA.ORB getORB()
    {
	return orb;
    }

    public static org.omg.PortableServer.POA getRootPOA()
    {
	return poa;
    }

    public static org.omg.CORBA.Object stringToObject(String ref) 
	    throws InvalidReferenceException
    {
	try {
	    return orb.string_to_object(ref);
	}
	catch (Exception e) {
	    throw new InvalidReferenceException(e);
	}
    }

    public static <Type extends org.omg.CORBA.Object> 
    Type stringToObject(Class<Type> type, String ref) 
	    throws InvalidReferenceException
    {
	try {
	    org.omg.CORBA.Object object = orb.string_to_object(ref);
	    return TypeHelper.narrow(type, object);
	}
	catch (Exception e) {
	    throw new InvalidReferenceException(e);
	}
    }

    public static String objectToString(org.omg.CORBA.Object obj, int ref_type)
    {
	String ref = new String();
	try {
	    ref = orb.object_to_string(obj);
	}
	catch (Exception e) {
	    return ref;
	}

	if (ref_type == CORBALOC)
	    return iorToCorbaloc(ref);

	return ref;
    }
    
    public static String constructReference(String partition, String type, String name, String host, int port)
    {
	return "corbaloc:iiop:" + host + ":" + port + "/%ff" + type + "%00" + partition + "/" + type + "/" + name;
    }
    
    static boolean equals(org.omg.CORBA.Object first, org.omg.CORBA.Object second)
    {
	return objectToString(first, Core.IOR).equals(objectToString(second, Core.IOR));
    }
    
    private static boolean isValidChar(char c)
    {
	return (Character.isLetterOrDigit(c) || c == ';' || c == '/'
		|| c == ':' || c == '?' || c == ':' || c == '@' || c == '&'
		|| c == '=' || c == '+' || c == '$' || c == ',' || c == '-'
		|| c == '_' || c == '!' || c == '~' || c == '*' || c == '\''
		|| c == '(' || c == ')');
    }

    private static String iorToCorbaloc(String iorstr)
    {
	IOR ior = parseIORString(iorstr);
	String result = new String("corbaloc:iiop:");

	for (int i = 0; i < ior.profiles.length; i++) {
	    if (ior.profiles[i].tag == TAG_INTERNET_IOP.value) {
		IIOPProfile profile = new IIOPProfile(
			ior.profiles[i].profile_data);
		IIOPAddress address = (IIOPAddress) profile.getAddress();
		byte key[] = profile.get_object_key();
		result += address.getIP() + ":" + address.getPort() + "/";

		for (int j = 0; j < key.length; j++) {
		    if (isValidChar((char) key[j])) {
			result += (char) key[j];
		    }
		    else {
			int ch = key[j] < 0 ? 256 + key[j] : key[j];
			result += "%" + HEXCHAR[ch / 16] + HEXCHAR[ch % 16];
		    }
		}
		break;
	    }
	}
	return result;
    }

    private static IOR parseIORString(String iorstr)
    {
	java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
	int cnt = (iorstr.length() - 4) / 2;
	for (int j = 0; j < cnt; j++) {
	    char c1 = iorstr.charAt(j * 2 + 4);
	    char c2 = iorstr.charAt(j * 2 + 5);
	    int i1 = (c1 >= 'a') ? (10 + c1 - 'a')
		    : ((c1 >= 'A') ? (10 + c1 - 'A') : (c1 - '0'));
	    int i2 = (c2 >= 'a') ? (10 + c2 - 'a')
		    : ((c2 >= 'A') ? (10 + c2 - 'A') : (c2 - '0'));
	    bos.write((i1 * 16 + i2));
	}

	org.jacorb.orb.CDRInputStream in = new org.jacorb.orb.CDRInputStream(
		orb, bos.toByteArray());

	boolean endianness = in.read_boolean();
	if (endianness)
	    in.setLittleEndian(true);

	return org.omg.IOP.IORHelper.read(in);
    }
}
