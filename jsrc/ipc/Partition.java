package ipc;

import org.omg.CosNaming.NamingContext;

/**
 * @author Serguei Kolos
 * 
 *         This class provides simple API for communicating with the
 *         Inter-Process Communication (IPC) Naming Service. It implements
 *         functions for accessing objects references, which are registered in
 *         the IPC domain, represented by an instance of this class. Each IPC
 *         domain has a unique name. Different instances of Partition class,
 *         having the same name, represent the same IPC domain.
 * 
 */
public class Partition
{
    private final String name;
    
    private static class ObjectResolver implements Cache.Resolver
    {
	String partition;
	String type;
	String name;
	org.omg.CORBA.Object object;

	ObjectResolver(String partition, String type, String name)
	{
	    this.partition = partition;
	    this.type = type;
	    this.name = name;
	}

	public org.omg.CORBA.Object resolve() throws InvalidPartitionException, InvalidObjectException
	{
	    return NamingService.resolve(this.partition, this.type, this.name);
	}
        
        public String uniqueName() {
            return partition + "/" + type + "/" + name;
        }
    }

    /**
     * Constructs the instance representing the <i>default</i> (sometimes
     * referenced as well as <i>initial</i>) IPC communication domain.
     */
    public Partition()
    {
	this(null);
    }

    /**
     * Constructs the instance representing the named IPC communication domain.
     * 
     * @param name partition name
     */
    public Partition(String name)
    {
	this.name = (name == null) ? ipc.partition.default_name : name;
    }

    /**
     * Checks validity of the IPC domain, represented the this instance. If the
     * IPC domain, represented by this object, exists, one can use
     * 
     * @return true if the IPC partition exists, false otherwise.
     */
    public boolean isValid()
    {
	try {
	    return isValid(NamingService.findNamingService(name));
	}
	catch (InvalidPartitionException e) {
	    return false;
	}
    }

    /**
     * Returns the name of the IPC domain represented by this instance.
     * 
     * @return partition name
     */
    public String getName()
    {
	return name;
    }

    /**
     * Checks validity of an object registered with this IPC domain. This
     * function first tries to obtain a CORBA reference to the object identified
     * by the given <tt>type</tt> and <tt>name</tt> and if reference exists,
     * sends remote, ping-like, request to this object to make sure that the
     * object is alive.
     * 
     * @param <Type>
     *            - type of the object. A valid type is a Java
     *            <tt>abstract interface</tt> which is generated from the
     *            respective IDL <tt>interface</tt> declaration. For example to
     *            check existence of the IS server called "RunParams" in the
     *            "ATLAS" partition (given that IS server implements the
     *            following IDL interface: 
     *            <pre>
     *            {@code 
     *            	module is {
     *            		interface repository {
     *            			... 
     *            		}; 
     *            	}; 
     *            }
     *            </pre>
     *            ) one has to use the following code:
     *            <pre>
     *            {@code
     * 	  		Partition p = new Partition("ATLAS");
     * 	  		boolean b = p.isObjectValid(is.repository.class, "RunParams");
     * 		   }
     * 		</pre>
     * @param type	object type
     * @param name	object name
     * @return true if the given object is registered with the IPC domain and
     *         responds to remote requests.
     * @throws InvalidPartitionException
     *             if partition does not exist
     */
    public <Type extends org.omg.CORBA.Object> boolean isObjectValid(
	    Class<Type> type, String name) throws InvalidPartitionException
    {
	try {
	    return isValid(lookup(type, name));
	}
	catch (InvalidObjectException e) {
	    return false;
	}
    }

    /**
     * Checks validity of an object registered with this IPC domain. This
     * function first tries to obtain a CORBA reference to the object identified
     * by the given <tt>type_name</tt> and <tt>name</tt> and if reference
     * exists, sends remote, ping-like, request to this object to make sure that
     * the object is alive.
     * <p>
     * This function should be used only when object type is not known during
     * compilation. Otherwise another the overloaded function with the object
     * type parameter should be preferred.
     * 
     * @param type_name	name of the obejct's type
     * @param name	object name
     * @return true if the given object is registered with the IPC domain and
     *         responds to remote requests.
     * @throws InvalidPartitionException
     *             if partition does not exist
     */
    public boolean isObjectValid(String type_name, String name)
	    throws InvalidPartitionException
    {
	try {
	    return isValid(lookup(type_name, name));
	}
	catch (InvalidObjectException e) {
	    return false;
	}
    }

    /**
     * Retrieves remote object reference from the IPC Naming Service. This
     * method does not check existence of the IPC object represented by this
     * reference.
     * 
     * @param type	object type
     * @param name	object name
     * @return reference to remote object
     * @throws InvalidPartitionException
     *             if partition does not exist
     * @throws InvalidObjectException
     *             if the target object is not registered with the current
     *             partition
     */
    public <Type extends org.omg.CORBA.Object> Type lookup(Class<Type> type,
	    String name) throws InvalidPartitionException, InvalidObjectException
    {
	try {
	    org.omg.CORBA.Object object = Cache.get(
            	new ObjectResolver(this.name, TypeHelper.name(type), name));
	    Type t_object = TypeHelper.narrow(type, object);
	    return t_object;
	}
	catch (Exception e) {
	    throw new InvalidObjectException(this.name, TypeHelper.name(type), name, e);
	}
    }

    /**
     * Retrieves remote object reference from the IPC Naming Service. This
     * method does not check existence of the IPC object represented by this
     * reference.
     * <p>
     * This function should be used only when object type is not known during
     * compilation. Otherwise another the overloaded function with the object
     * type parameter should be preferred.
     * 
     * @param type_name	object's type name
     * @param name	object's name
     * @return reference to remote object
     * @throws InvalidPartitionException
     *             if partition does not exist
     * @throws InvalidObjectException
     *             if the target object is not registered with the current
     *             partition
     */
    public ipc.servant lookup(String type_name, String name)
	    throws InvalidPartitionException, InvalidObjectException
    {
	try {
	    org.omg.CORBA.Object object = Cache.get(new ObjectResolver(this.name, type_name, name));
	    return ipc.servantHelper.narrow(object);
	}
	catch (Exception e) {
	    throw new InvalidObjectException(this.name, type_name, name, e);
	}
    }

    private boolean isValid(org.omg.CORBA.Object object)
    {
	if (object == null) {
	    return false;
	}
	try {
	    if (object._non_existent() == true) {
		return false;
	    }
	}
	catch (Exception e) {
	    return false;
	}
	return true;
    }
}
