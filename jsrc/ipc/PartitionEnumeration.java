package ipc;

import org.omg.CosNaming.*;
import java.util.Vector;

/**
 * @author Serguei Kolos
 * This class can be used to enumerate all known IPC partitions.
 * A constructor of this class retrieves partition references from the
 * IPC Naming Service. 
 * <p>This class provides two ways of iterating over its elements:
 * <ul>
 *	<li>by implementing the java.util.Enumeration interface<Partition>
 * 	<li>by extending the Vector<Partition> class
 * </ul>	
 */
public class PartitionEnumeration extends Vector<Partition>
	implements java.util.Enumeration<Partition>
{
    private int pos;

    /**
     * Enumerates all existing partitions.
     * 
     * @throws InvalidPartitionException	if the initial partition does not exist
     */
    public PartitionEnumeration() throws InvalidPartitionException
    {
	Binding[] bl = NamingService.listBindings((new Partition()).getName(),
		null);
	for (int i = 0; i < bl.length; i++) {
	    if (bl[i].binding_type == BindingType.nobject) {
		Partition p = new Partition(bl[i].binding_name[0].id);
		if (p.isValid()) {
		    add(p);
		}
	    }
	}
	pos = 0;
    }

    /**
     * @return	if and only if this enumeration object contains 
     * 		at least one more element to provide; false otherwise.
     */
    public boolean hasMoreElements()
    {
	return (pos < size());
    }

    /**
     * Returns the next element of this enumeration if this enumeration 
     * object has at least one more element to provide.
     * @return the next element of this enumeration.
     * 
     * @throws NoSuchElementException	if no more elements exist.     
     */
    public Partition nextElement()
    {
	try {
	    return get(pos++);
	}
	catch(Exception e) {
	    throw new java.util.NoSuchElementException();
	}
    }

    /**
     * Resets the enumeration's current position <i>before</i> the first element, 
     * i.e. to the state which this object had just after construction.
     */
    public void reset()
    {
	pos = 0;
    }
}
