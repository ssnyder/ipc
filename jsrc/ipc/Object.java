package ipc;

import java.lang.reflect.*;
import ers.*;

/**
 * 
 * @author Serguei Kolos
 * 
 * This class is designed for simplifying implementation of interfaces
 * declared in CORBA IDL. User has to inherit an IDL interface implementation 
 * class from this one in order to have it implicitly linked with a suitable 
 * Portable Object Adapter(POA). This way a new instance of a user class
 * will be automatically activated with this POA at appropriate time.
 * <p>
 * Note that this class can be used as well for implementing IDL interfaces,
 * which inherit the <tt>ipc::servant</tt> base interface. The <tt>ipc::servant</tt> 
 * interface defines few common operations for remote objects: pinging, shutting down
 * and retrieving context of the application where the object resides. This class
 * provides default implementation for those operations, while a user specific 
 * class may override them is required.
 *
 * @param <Type> type of CORBA interface, which is implemented by this object. 
 * 		A valid type is a Java <tt>abstract interface</tt> which is 
 * 		generated from the respective IDL <tt>interface</tt> declaration.
 * For example to implement the <tt>is::callback</tt> IDL interface
 * <pre>
 * {@code
 * 	module is {
 * 		interface callback {
 * 			void inform();
 * 		};
 * 
 * 		interface repository {
 * 			void subscribe(in callback cb);
 * 		}
 * 	};
 * }
 * </pre>
 * one should declare a new Java class inheriting from the ipc.Object and implementing
 * functions declared in the <tt>is::callback</tt> interface:
 * <pre>
 * {@code
 * 	class Callback extends Object<is.callback> {
 * 		public void inform() { ... }
 * 	}
 * }
 * </pre>
 * An instance of the Callback class can be used as shown in the following code fragment:
 * <pre>
 * {@code
 * 	ipc.Partition p = new Partition("ATLAS");
 * 	is.repository rep = p.lookup(is.repository.class, "RunParams");
 * 	Callback cb = new Callback();
 * 	rep.subscribe(cb._this());
 * </pre>
 * }
 * 
 */
public class Object<T extends ipc.servant> extends ipc.servantPOA
{
    protected static class ObjectNotActive extends RuntimeException{}
    
    private static int getPid()
    {
	String processName = java.lang.management.ManagementFactory
		.getRuntimeMXBean().getName();
	return Integer.parseInt(processName.split("@")[0]);
    }

    private static String getHostname()
    {
	String hostname = "unknown";
	try {
	    java.net.InetAddress addr = java.net.InetAddress.getLocalHost();
	    hostname = addr.getHostName();
	}
	catch (java.net.UnknownHostException e) {
	}

	return hostname;
    }

    protected static final ipc.servantPackage.ApplicationContext appcontext 
    	= new ipc.servantPackage.ApplicationContext(
    		java.lang.System.getenv("TDAQ_APPLICATION_NAME") != null 
    			? java.lang.System.getenv("TDAQ_APPLICATION_NAME")
    			: "unknown", java.lang.System.getProperty("user.name", "unknown"),
    			getHostname(), 
    			(int) (java.lang.System.currentTimeMillis() / 1000),
    			getPid(), 
    			(short) ers.Configuration.instance().debug_level(), 
    			(short) ers.Configuration.instance().verbosity_level());

    protected ipc.servantPackage.PartitionContext partition_context;
    
    /**
     * This object contains the type of the Java interface which is implemented by this Object.
     */
    protected final Class<T> type;
    
    /**
     * This is a CORBA reference to the CORBA object implemented by this class. 
     */
    protected T reference;

    @SuppressWarnings("unchecked")
    public Object()
    {
	this.type = (Class<T>) ((ParameterizedType) this.getClass()
		.getGenericSuperclass()).getActualTypeArguments()[0];
    }

    Object(String partition_name, String object_name)
    {
	this();
        partition_context = 
        	new ipc.servantPackage.PartitionContext(partition_name, object_name);
    }
    
    /**
     * Activates this object with the CORBA Portable Object Adapter and returns
     * a CORBA reference to this object. The reference can be passed to another 
     * process, which can use it for communicating with this object.
     * @return	CORBA reference to this object
     */
    public T _this()
    {
	activate();
	return reference;
    }

    public ipc.servantPackage.ApplicationContext app_context()
    {
	return appcontext;
    }

    public ipc.servantPackage.PartitionContext partition_context()
    {
	return partition_context;
    }

    public short debug_level()
    {
	return (short)ers.Configuration.instance().debug_level();
    }

    public void debug_level(short arg)
    {
	ers.Configuration.instance().debug_level(arg);
    }

    public short verbosity_level()
    {
	return (short)ers.Configuration.instance().verbosity_level();
    }

    public void verbosity_level(short arg)
    {
	ers.Configuration.instance().verbosity_level(arg);
    }

    public void ping()
    {
	;
    }

    public void shutdown()
    {
	;
    }

    public void test(String param)
    {
	;
    }
    
    private void activate()
    {
	if (reference != null) {
	    return;
	}
	
	org.omg.CORBA.ORB orb = ipc.Core.getORB();
	try {
	    Class<?> helper = Class.forName(type.getName() + "POATie");
	    Class<?> param = Class.forName(type.getName() + "Operations");
	    org.omg.PortableServer.Servant servant = (org.omg.PortableServer.Servant) helper
		    .getConstructor(param).newInstance(this);
	    reference = TypeHelper.narrow(type, servant._this_object(orb));
	}
	catch (Exception e) {
	    try {
		reference = TypeHelper.narrow(type, _this_object(orb));
	    }
	    catch(InvalidReferenceException e1) {
		throw new RuntimeException("Object activation failed", e1);
	    }
	}
    }
    
    protected void finalize() throws Throwable
    {
	if (reference == null) {
	    return;
	}

	try {
	    ipc.Core.getRootPOA().deactivate_object(
		    ipc.Core.getRootPOA().reference_to_id(reference));
	    reference = null;
	}
	catch (Exception e) {
	    ers.Logger.log("Object deactivation failed: " + e);
	}
    }    
}
