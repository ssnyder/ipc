#include <stdlib.h>

#include <ers/ers.h>

#include <ipc/exceptions.h>
#include <ipc/core.h>
#include <ipc/cache.h>
#include <ipc/policy.h>
#include <ipc/util.h>

#include <omniORB4/minorCode.h>

#ifdef minor
#undef minor
#endif

ERS_DECLARE_ISSUE( ipc, NotInCache, "IPC cache does not contain given object", ERS_EMPTY)

CORBA::Object_ptr IPCCache::Entity::getObject()
{
    boost::mutex::scoped_lock ml(m_mutex);

    if (CORBA::is_nil(m_object)) {
	m_object = m_resolver();

	if (!CORBA::is_nil(m_object)) {
	    ERS_DEBUG( 2, "New object reference was found");
	    omniORB::installTimeoutExceptionHandler(m_object, this,
		    timeout_handler);
            omniORB::installTransientExceptionHandler(m_object, this,
                    transient_handler);
	    omniORB::installCommFailureExceptionHandler(m_object, this,
		    commfailure_handler);
	    omniORB::installSystemExceptionHandler(m_object, this,
		    system_handler);
	}
    }

    return CORBA::Object::_duplicate(m_object);
}

bool IPCCache::Entity::recover(bool is_timedout)
{
    CORBA::Object_var new_ref = CORBA::Object::_nil();

    boost::mutex::scoped_lock ml(m_mutex);

    try {
	new_ref = m_resolver();
    }
    catch (daq::ipc::Exception & ex) {
	ERS_DEBUG( 2, "Recovery failed: " << ex);
    }

    if (!CORBA::is_nil(new_ref) && !new_ref->_is_equivalent(m_object)) {
	if (!CORBA::is_nil(m_object)) {
	    ERS_DEBUG( 2, "New object reference is different from the old one");
	    omni::locationForward(m_object->_PR_getobj(),
		    CORBA::Object::_duplicate(new_ref)->_PR_getobj(), true);
	}
	else {
	    ERS_DEBUG( 2, "Replacing nil reference with the new one");
	    m_object = CORBA::Object::_duplicate(new_ref);
	    omniORB::installTimeoutExceptionHandler(m_object, this,
		    timeout_handler);
            omniORB::installTransientExceptionHandler(m_object, this,
                    transient_handler);
	    omniORB::installCommFailureExceptionHandler(m_object, this,
		    commfailure_handler);
	    omniORB::installSystemExceptionHandler(m_object, this,
		    system_handler);
	}
	ERS_DEBUG( 2, "The old reference was replaced with the new one");
	return true;
    }
    else {
	ERS_DEBUG( 2,
		"The new object reference is "
		    << ( CORBA::is_nil(new_ref) ? "nil" : "the same as the old one" ));
        return !CORBA::is_nil(new_ref) && !is_timedout;
    }
}

IPCCache::IPCCache()
{
    omniORB::installTransientExceptionHandler(0, global_transient_handler);
}

IPCCache::~IPCCache()
{
    clear();
}

void IPCCache::clear() {
    boost::mutex::scoped_lock lock(m_mutex);
    m_map.clear();
}

CORBA::Object_ptr 
IPCCache::find(const std::string & reference, const Resolver & resolver)
{
    std::map<std::string, Entity>::iterator it;
    {
	boost::mutex::scoped_lock lock(m_mutex);
	it = m_map.find(reference);

	if (it == m_map.end()) {
	    it = m_map.insert(std::make_pair(reference, Entity(resolver))).first;
	}
    }

    CORBA::Object_ptr object = it->second.getObject();

    ERS_DEBUG( 3,
	    "The reference for the '" << reference << "' object "
		<< ( CORBA::is_nil( object ) ? "does not exist in cache" : "is found in cache" ));

    return object;
}

CORBA::Boolean IPCCache::handleException(IPCCache::Entity * entity,
	CORBA::ULong retries, bool is_timedout)
{
    try {
	if (!retries) {
	    ERS_DEBUG( 2,
		    "First invocation on the remote object reference fails, try to recover ... ");

	    return entity->recover(is_timedout);
	}
	else {
	    ERS_DEBUG( 2,
		    "Second invocation on the remote object reference fails, giving up ... ");
	}
    }
    catch (ipc::NotInCache & ex) {
	ERS_DEBUG( 2, "Object is not found in cache");
    }
    return 0;
}

CORBA::Boolean IPCCache::global_transient_handler(void *, CORBA::ULong retries,
	const CORBA::TRANSIENT & ex)
{
    ERS_DEBUG( 2,
	    "handler invoked with '" << ( ex.NP_minorString()
		    ? ex.NP_minorString() : "Unknown" ) << "' minor code");

    if (ex.minor() == omni::TRANSIENT_FailedOnForwarded) {
	ERS_DEBUG( 2, "Request to the forwarded object failed");
	if (!retries && ex.completed() == CORBA::COMPLETED_NO) {
	    ERS_DEBUG( 2,
		    "Try to invoke the same request using the original object reference");
	    return 1;
	}
    }

    ERS_DEBUG( 2, "Propagate exception to the client");
    return 0;
}

CORBA::Boolean IPCCache::transient_handler(void * cookie, CORBA::ULong retries,
	const CORBA::TRANSIENT & ex)
{
    if (ex.minor() == omni::TRANSIENT_FailedOnForwarded) {
	ERS_DEBUG( 2, "Request to the forwarded object failed");
	if (!retries && ex.completed() == CORBA::COMPLETED_NO) {
	    ERS_DEBUG( 2,
		    "Try to invoke the same request using the original object reference");
	    return 1;
	}
    }

    return system_handler(cookie, retries, ex);
}

CORBA::Boolean IPCCache::timeout_handler(void * cookie, CORBA::ULong retries,
        const CORBA::TIMEOUT& ex)
{
    return system_handler(cookie, retries, ex);
}

CORBA::Boolean IPCCache::commfailure_handler(void * cookie,
	CORBA::ULong retries, const CORBA::COMM_FAILURE & ex)
{

    return system_handler(cookie, retries, ex);
}

CORBA::Boolean IPCCache::system_handler(void * cookie, CORBA::ULong retries,
	const CORBA::SystemException & ex)
{
    ERS_DEBUG( 2,
	    "handler invoked for the '" << ex._name() << "' exception with minor code "
		<< ( ex.NP_minorString() ? ex.NP_minorString() : "Unknown" ));

    CORBA::CompletionStatus status = ex.completed();
    if (status == CORBA::COMPLETED_YES) {
	// request has been delivered to the remote object and we don't
	// know whether it has been already processed or not.
	// It might be dangerous to retry.
	ERS_DEBUG( 2,
		"Exception status is " << status << ", propagate " << ex._name() << " exception to user code");
	return 0;
    }

    return IPCSingleton<IPCCache>::instance().handleException(
	    (IPCCache::Entity *) cookie, retries, ex.minor() == omni::TRANSIENT_CallTimedout);
}
