////////////////////////////////////////////////////////////////////////////
//      client_interceptor.cc
//
//      Implementation of the IPC proxy interceptor
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains implementation of the IPC proxy client
//
////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>

#include <ers/ers.h>
#include <ipc/interceptors.h>
#include <ipc/util.h>
#include <ipc/core.h>
#include <ipc/object.h>

#include <proxy/proxy.hh>

#include <omniORB4/omniIOR.h>
#include <omniORB4/omniURI.h>
#include <omniORB4/minorCode.h>

namespace
{
    std::string recover_interface_id( const omniIOR & ior )
    {
	const IOP::TaggedProfileList& profiles = ior.iopProfiles();
	_CORBA_Unbounded_Sequence_Octet object_key;
	for ( size_t i = 0; i < profiles.length(); i++ )
	{
	    if ( profiles[i].tag == IOP::TAG_INTERNET_IOP )
	    {
		IIOP::unmarshalObjectKey( profiles[i], object_key );
	    }
	}
	
        size_t end = 1;
        for ( size_t i = 1; i < object_key.length(); i++ )
	{
	    if ( !ipc::util::isValidChar( object_key[i] ) )
	    {
		end = i;
                break;
	    }
	}
        
        std::string result;
        if ( end > 1 )
        {
            result = "IDL:" + std::string( (const char *)object_key.get_buffer(), (size_t)1, end - 1 ) + ":1.0";
        }
	return result;
    }
}

class IPCClientProxyInterceptor : public IPCClientInterceptor
{
    boost::mutex				mutex_;
    ipc::proxy_var				proxy_;
    std::map<std::string, CORBA::Object_var>	cache_;
    std::map<std::string, int>			supported_;
    bool					no_proxy_;
    
  public:
    IPCClientProxyInterceptor();
  
  private:
    
    typedef std::pair<IPCClientProxyInterceptor*,CORBA::Object_ptr> ExceptionCookie;
    
    bool isProxy( const omniIOR & ior ) const;
           
    std::string ior_to_string( const omniIOR & ior );
    
    void sendRequest( const omniIOR & ior, const char * interface_id, const char * operation_id, IOP::ServiceContextList & );
};

IPCClientProxyInterceptor::IPCClientProxyInterceptor()
  : no_proxy_( false )
{
    try {
	std::string reference = ipc::util::constructReference(	    ipc::proxy::name,
								    ipc::util::getTypeName<ipc::proxy>(),
								    IPC_PROXY_CONNECT_FILE_NAME );

	CORBA::Object_var object = IPCCore::stringToObject( reference );
	proxy_ = ipc::proxy::_narrow( object );
        
	if ( CORBA::is_nil( proxy_ ) )
	{
	    throw daq::ipc::IgnorePluginException( ERS_HERE, "ipcproxyicp", "Can not connect to the IPC proxy application" );
	}

        ipc::proxy::list_var interfaces;
        
        proxy_ -> get_supported_interfaces( interfaces );
        
        for ( size_t i = 0; i < interfaces->length(); i++ )
        {
	    supported_.insert( std::make_pair( std::string( interfaces[i] ), 0 ) );
	}
    }
    catch( daq::ipc::Exception & ex ) {
	throw daq::ipc::IgnorePluginException( ERS_HERE, "ipcproxyicp", "Can not connect to the IPC proxy application", ex );
    }
    catch( CORBA::SystemException & ex ) {
	throw daq::ipc::IgnorePluginException( ERS_HERE, "ipcproxyicp", "Can not connect to the IPC proxy application",
					    daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
    }

    if ( !supported_.size() )
    {
	throw daq::ipc::IgnorePluginException( ERS_HERE, "ipcproxyicp", "IPC proxy application supports no interfaces" );
    }
}

bool
IPCClientProxyInterceptor::isProxy( const omniIOR & ior ) const
{
    const omni::giopAddressList & list = ior.getIORInfo()->addresses();
    for ( size_t i = 0; i < list.size(); i++ )
    {
	const char * address = list[i]->address();
	size_t len = strlen( address );
	ERS_DEBUG( 3, "checking '" << address << "' address" );
	if ( address[len-2] == ':' && address[len-1] == '0' )
        {
	    ERS_DEBUG( 3, "the given reference represents the local proxy address" );
            return true;
        }
    }
    ERS_DEBUG( 3, "the given reference represents the remote address" );
    return false;
}

std::string
IPCClientProxyInterceptor::ior_to_string( const omniIOR & ior )
{
    std::vector<std::pair<std::string,int> > endpoints;

    const IOP::TaggedProfileList& profiles = ior.iopProfiles();
    _CORBA_Unbounded_Sequence_Octet object_key;
    for ( size_t i = 0; i < profiles.length(); i++ )
    {
	if ( profiles[i].tag == IOP::TAG_INTERNET_IOP )
	{
	    IIOP::unmarshalObjectKey( profiles[i], object_key );
	}
    }

    std::ostringstream	out;
    out << "corbaloc:";

    omniIOR::IORInfo * info = ior.getIORInfo();
    const omni::giopAddressList & list = info->addresses();
    for ( size_t i = 0; i < list.size(); i++ )
    {
	std::string address = list[i]->address();
        if ( address.find( "giop:tcp" ) == 0 )
        {
	    std::string ip = address.substr( 9, address.find( ':', 9 ) );
            out << "iiop:" << ip;
        }
	else if ( address.find( "giop:unix" ) == 0 )
        {
	    std::string file = address.substr( 10, address.find( ':', 10 ) );
	    out << "omniunix:" << file << ":";
	}
	if ((i+1) < list.size())
	    out << ",";
    }

    out << '/';
    for ( size_t i = 0; i < object_key.length(); i++ )
    {
	if ( ipc::util::isValidChar( object_key[i] ) )
	{
	    out << object_key[i];
	}
	else
	{
	    out << "%" << std::hex << std::setw(2) << std::setfill( '0' ) << (int)object_key[i];
	}
    }

    return out.str();
}

void 
IPCClientProxyInterceptor::sendRequest( const omniIOR & ior, const char * interface_id, const char * , IOP::ServiceContextList & )
{
    if ( no_proxy_ )
    {
	ERS_DEBUG( 2, "One of the previous attempts to connect to the proxy failed, no further attempts will be made." );
        return;
    }
    
    if ( !isProxy( ior ) )
    {
	std::string ref = ior_to_string( ior );
	ERS_DEBUG( 2, "detected request to the '" << ref << "' remote object reference" );
	
        boost::mutex::scoped_lock lock( mutex_ );
	std::map<std::string, CORBA::Object_var>::iterator it = cache_.find( ref );

	if ( it != cache_.end() )
	{
	    ERS_DEBUG( 2, "the proxy object reference has been found in the cache" );
	    if ( CORBA::is_nil( it->second ) )
            {
		ERS_DEBUG( 2, "this object can not be handled by proxy because its type is unknown" );
            	return;
            }
            
	    ERS_DEBUG( 2, "checking existence of the remote object ... " );
            bool object_not_exist = false;
            try {
		object_not_exist = it -> second -> _non_existent();
	    }
	    catch( CORBA::OBJECT_NOT_EXIST & ) {
            	object_not_exist = true;
            }
	    catch( CORBA::SystemException & ) {
		ERS_DEBUG( 2, "can not contact proxy server (it is not running?)" );
                cache_.erase( it );
		no_proxy_ = true;
                return;
            }
	    
            if ( object_not_exist )
            {
		ERS_DEBUG( 2, "the proxy server is running but the remote object does not exist, "
				"removing proxy object from cache" );
                cache_.erase( it );
	    }
	    else
            {
		ERS_DEBUG( 2, "the remote object exists" );
		throw omniORB::LOCATION_FORWARD( CORBA::Object::_duplicate( it -> second ), 0 );
	    }
	}
        
	ERS_DEBUG( 2, "the proxy object reference does not exist in the cache" );

        std::string IID = strlen( interface_id ) ? interface_id : recover_interface_id( ior );
        ERS_DEBUG( 2, "the given object type is '" << interface_id << "' but the '" << IID << "' will be used" );
        
	if ( supported_.find( IID ) == supported_.end() )
	{
	    ERS_DEBUG( 2, "interface '" << IID << "' is not supported by the proxy" );
	    return;
	}
        
	CORBA::Object_ptr object;
	try
	{
	    ERS_DEBUG( 1, "asking proxy server to create new proxy object" );
	    object = proxy_->create( IID.c_str(), ref.c_str() );
	    
	    ERS_DEBUG( 1, "new proxy object has been created" );
            
	    cache_.insert( std::make_pair( ref, object ) );
	}
	catch( ipc::proxy::InterfaceNotSupported & ex )
	{
	    ERS_DEBUG( 1, "interface '" << IID << "' is not supported by the proxy" );
	    return;
	}
	catch( ipc::proxy::TargetDoesNotExist & ex )
	{
	    ERS_DEBUG( 1, "the '" << ref << "' remote object does not exist" );
	    return;
	}
	catch( ipc::proxy::BadReference & ex )
	{
	    ERS_DEBUG( 1, "the '" << ex.reference << "' remote object reference does not support the '" << ex.interface_id << "'" );
	    cache_.insert( std::make_pair( ref, CORBA::Object::_nil() ) );
	    return;
	}
	catch( CORBA::SystemException & ex )
	{
	    ERS_DEBUG( 1, "System exception '" << ex << "' has been received" );
	    no_proxy_ = true;
            return;
	}
	
        throw omniORB::LOCATION_FORWARD( CORBA::Object::_duplicate( object ), 0 );
    }
}

extern "C" void * create_ipcproxyicp()
{
    return new IPCClientProxyInterceptor;
}
