#include <ers/ers.h>
#include <ipc/interceptors.h>
#include <iostream>

class IPCServerTrace : public IPCServerInterceptor
{
    void receiveRequest( const char * interface_id, const char * operation_id, IOP::ServiceContextList & )
    {
    	ERS_LOG( operation_id << " request received for the object of \"" << interface_id << "\" type" );
    }
};

extern "C" void * create_ipcsrvtrace()
{
    return new IPCServerTrace;
}
