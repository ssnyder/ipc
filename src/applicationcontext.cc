#include <ipc/applicationcontext.h>
#include <ipc/exceptions.h>

namespace
{
    ipc::servant::ApplicationContext * get_info( ipc::servant_ptr servant )
    {
        if ( CORBA::is_nil( servant ) ) {
            return 0;
        }
        
        try {
            return servant -> app_context();
        }
        catch ( CORBA::SystemException & ex ) {
            return 0;
        }
    }
}

using std::chrono::system_clock;

IPCApplicationContext::IPCApplicationContext(ipc::servant::ApplicationContext* p)
: ipc::servant::ApplicationContext_var( p ),
  m_valid( operator->() ),
  m_name( m_valid ? (const char*)in().name : "" ),
  m_owner( m_valid ? (const char*)in().owner : ""),
  m_host( m_valid ? (const char*)in().host : "" ),
  m_time( system_clock::from_time_t( m_valid ? in().time : 0 ) ),
  m_pid( m_valid ? in().pid : 0 ),
  m_debug_level( m_valid ? in().debug_level : 0 ),
  m_verbosity_level( m_valid ? in().verbosity_level : 0 )
{
  out();
}

IPCApplicationContext::IPCApplicationContext( ipc::servant_ptr servant )
  : IPCApplicationContext( get_info( servant ) )
{ }

IPCApplicationContext::IPCApplicationContext( POA_ipc::servant & servant )
  : IPCApplicationContext( servant.app_context() )
{ }
