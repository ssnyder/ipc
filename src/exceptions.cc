/*
 *  exceptions.cc
 *  ipc
 *
 *  Modified by Serguei Kolos on 24.08.05.
 *  Copyright 2005 CERN. All rights reserved.
 *
 */
#include <stdio.h>

#include <ipc/exceptions.h>
#include <omniORB4/minorCode.h>

#undef minor

namespace
{
    class Generic : public CORBA::SystemException
    {
	std::string name_;
	std::string repoid_;
	std::string typeid_;
        std::string mname_;
      public:
	inline Generic( const char * name, CORBA::ULong minor, const char * mname )
	  : CORBA::SystemException( minor, CORBA::COMPLETED_NO ),
            name_( name ),
            repoid_( "IDL:omg.org/CORBA/" + name_ + ":1.0" ),
            typeid_( "Exception/CorbaSystemException/__non_valid_exception_name__" ),	// _downcast function must return 0
            mname_( mname )
        { ; }
        
	virtual void _raise() const 
        { throw *this; }
        
 	virtual const char* _name() const
        { return name_.c_str(); }
        
        virtual const char* _NP_repoId( int * size ) const {
	    *size = repoid_.size();
	    return repoid_.c_str();
        }
	
        virtual const char* NP_minorString() const
        { return mname_.c_str(); }
        
      private:
 	virtual Exception* _NP_duplicate() const
        { return new Generic( *this ); }
 	
        virtual const char* _NP_typeId() const
        { return typeid_.c_str(); }
    };
}

namespace
{
    void read( std::istream & in, char separator, std::string & input )
    {
	char ch;
	while ( !(in >> ch).fail() ) {
	    if ( ch == separator ) {
		break;
	    }
	    else {
		input += ch;
	    }
	}
    }
}

std::ostream & 
CORBA::operator<< ( std::ostream & out, const CORBA::SystemException * ex )
{
    const char * minor_name = ex -> NP_minorString() ? ex -> NP_minorString() : "Unknown";
    out << ex -> _name() << "(" << ex -> minor() << "=" << minor_name << ")";
    return out;
}

std::istream & 
CORBA::operator>> ( std::istream & in, CORBA::SystemException * & ex )
{
    std::string name;
    std::string errcode_name;
    std::string errcode_val;
    CORBA::ULong errcode;
    
    read( in, '(', name );
    read( in, '=', errcode_val );
    read( in, ')', errcode_name );
    sscanf( errcode_val.c_str(), "%u", &errcode );
    
    ex = new Generic( name.c_str(), errcode, errcode_name.c_str() );
    return in;
}

std::ostream & 
operator<< ( std::ostream & out, const CORBA::SystemException & ex )
{
    out << &ex;
    return out;
}

