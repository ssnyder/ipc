#include <cstdio>

#include <ipc/object.h>
#include <ipc/core.h>

#include <ipc/server/backup.h>

using namespace std;

namespace {
    const std::string name_suffix = ".shadow";
}

bofstream::bofstream(const string & fname)
  : m_name(fname), m_bck_name(m_name + name_suffix)
{
    if (rename(m_name.c_str(), m_bck_name.c_str())) {
	ERS_LOG("Can not rename '" + m_name + "' backup file: " + strerror(errno));
    }
    this->open(m_name, ofstream::out | ofstream::trunc);
    exceptions( ios::badbit | ios::failbit | ios::eofbit );
}

bofstream::~bofstream()
{
    try {
	this->close();
	if (remove(m_bck_name.c_str())) {
	    ERS_LOG("Can not remove '" + m_bck_name + "' backup file: " + strerror(errno));
	}
    } catch (std::exception & ex) {
	ERS_LOG("Can not properly close the '" + m_name + "' backup file: " + strerror(errno));
    }
}

bifstream::bifstream(const string & fname)
  : m_name(fname), m_bck_name(m_name + name_suffix)
{
    this->open(m_bck_name);
    if (good()) {
	this->close();
	if (rename(m_bck_name.c_str(), m_name.c_str())) {
	    ERS_LOG("Can not rename '" + m_bck_name + "' backup file: " + strerror(errno));
	    this->open(m_bck_name);
	} else {
	    this->open(m_name);
	}
    } else {
	this->open(m_name);
    }
    exceptions( ios::badbit | ios::failbit | ios::eofbit );
}

namespace CosNaming
{
    ostream& operator<<=( ostream& out, const string & str )
    {
        unsigned int size = str.size();
        out.write( (const char*)&size, sizeof( size ) );
        out.write( (const char*)str.c_str(), size );
        return out;
    }

    istream& operator>>=( istream& in, string &str )
    {
        unsigned int size = 0;
        in.read( (char*)&size, sizeof( size ) );

        char * buff = new char[size + 1];
        in.read( buff, size );
        buff[size] = 0;

        str = buff;
        delete[] buff;
        return in;
    }
    
    ostream& operator<<=( ostream& out, const CosNaming::String & str )
    {
        unsigned int size = str.size();
        out.write( (const char*)&size, sizeof( size ) );
        out.write( (const char*)str.c_str(), size );
        return out;
    }

    istream& operator>>=( istream& in, CosNaming::String &str )
    {
        unsigned int size = 0;
        in.read( (char*)&size, sizeof( size ) );

        char * buff = new char[size + 1];
        in.read( buff, size );
        buff[size] = 0;

        str = String( buff );
        return in;
    }

    ostream& operator<<=( ostream& out, const ipc::URI & uri )
    {
        unsigned int size = uri.length();
        out.write( (const char*)&size, sizeof( size ) );
        out.write( (const char*)uri.get_buffer(), size );
        return out;
    }

    istream& operator>>=( istream& in, ipc::URI & uri )
    {
        unsigned int size = 0;
        in.read( (char*)&size, sizeof( size ) );

        uri.length( size );
        in.read( (char*)uri.get_buffer(), size );

        return in;
    }

    ostream& operator<<=( ostream& out, const CORBA::String_member & vv )
    {
	unsigned int length = strlen(vv);
	out.write( (const char *)&length, sizeof(length) );
	out.write( (const char *)vv, length );
	return out;
    }

    istream& operator>>=( istream& in, CORBA::String_member & vv )
    {
	unsigned int length = 0;
	in.read( (char *)&length, sizeof(length) );
	char * buf = new char[length+1];
	in.read( (char *)buf, length );
	buf[length] = 0;
	vv = buf;
	return in;
    }
    
    ostream& operator<<=( ostream & out, const BindingType & type )
    {
	out.write( (const char*)&type, sizeof( type ) );
	return out;
    }

    istream& operator>>=( istream & in, BindingType & type )
    {
	in.read( (char*)&type, sizeof( type ) );
	return in;
    }

    ostream& operator<<=( ostream & out, const Type & type )
    {
	out.write( (const char*)&type, sizeof( type ) );
	return out;
    }

    istream& operator>>=( istream & in, Type & type )
    {
	in.read( (char*)&type, sizeof( type ) );
	return in;
    }

    ostream& operator<<=( ostream & out, const NameComponent & key )
    {
	out <<= key.id;
	out <<= key.kind;
	return out;
    }

    istream& operator>>=( istream & in, NameComponent & key )
    {
	in >>= key.id ;
	in >>= key.kind;
	return in;
    }

    ostream& operator<<=( ostream & out, const Name & name )
    {
        unsigned int length = name.length();
        out.write( (const char*)&length, sizeof( length ) );
        for ( size_t i = 0; i < length; i++ )
            out <<= name[i];
        return out;
    }

    istream& operator>>=( istream & in, Name & name )
    {
	unsigned int length = 0;
	in.read( (char *)&length, sizeof(length) );
        name.length( length );
        for ( size_t i = 0; i < length; i++ )
            in >>= name[i];
	return in;
    }
}
