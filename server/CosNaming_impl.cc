////////////////////////////////////////////////////////////////////////////
//
//      CosNaming_impl.cc
//
//      source file for IPC library based on CosNaming service
//
//      Sergei Kolos May 1998
//
//	description:
//		This file contains implementation of the CORBA Naming service
//
////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <sstream>

#include <ipc/server/CosNaming_impl.h>
#include <ipc/server/backup.h>
#include <ipc/core.h>

PortableServer::POA_ptr	CosNaming::NamingContext_impl::the_poa;
unsigned int CosNaming::NamingContext_impl::the_global_request_counter;
unsigned int CosNaming::NamingContext_impl::the_modifying_request_counter;

namespace
{
    std::string
    print_name( const CosNaming::Name & n )
    {
	std::ostringstream out;
	for( size_t i = 0; i < n.length() - 1; i++)
	{
	    out << n[i].id << ":";
	}
	if ( n.length() )
	{
	    out << n[n.length()-1].id;
	}
	return out.str();
    }
}

using namespace CosNaming;

CORBA::Object_ptr 
Element::obj() const {
    if ( !CORBA::is_nil( m_obj ) )
	return ( m_obj );
        
    if ( m_uri.length() )
    {
	try {
	    m_obj = IPCCore::getORB() -> string_to_object( (const char *)m_uri.get_buffer() );
	}
	catch( daq::ipc::Exception & ex ) {
	    ers::error( ex );
	}
    }
    return m_obj;
}

ipc::URI * 
Element::ref_dup() {
    if ( m_uri.length() )
	return new ipc::URI( m_uri );
        
    if ( is_obj() || is_nc() )
    {
	try {
	    char * s = IPCCore::getORB() -> object_to_string( is_obj() ? (CORBA::Object_ptr)m_obj : (CORBA::Object_ptr)m_nc );
	    CORBA::ULong l = strlen( s ) + 1;
            m_uri.replace( l, l, (CORBA::Octet*)s, true );
	}
	catch( daq::ipc::Exception & ex ) {
	    ers::error( ex );
	}
    }
    return new ipc::URI( m_uri );
}

void 
Element::write( std::ostream & out ) const
{
    if ( m_uri.length() )
    {
    	out <<= m_uri;
        return ;
    }

    if ( is_obj() || is_nc() )
    {
	try {
	    char * s = IPCCore::getORB() -> object_to_string( is_obj() ? (CORBA::Object_ptr)m_obj : (CORBA::Object_ptr)m_nc );
	    CORBA::ULong l = strlen( s ) + 1;
            m_uri.replace( l, l, (CORBA::Octet*)s, true );
	}
	catch( daq::ipc::Exception & ex ) {
	    ers::error( ex );
            
	}
    }
    out <<= m_uri;
}

 //---------------------------------------
 // NamingContext_impl methods
 //---------------------------------------
 
NamingContext_impl::~NamingContext_impl()
{
    boost::mutex::scoped_lock rl( m_mutex );
    BindingMap::iterator it = m_bindings.begin();
    
    while( it != m_bindings.end() )
    {
    	BindingMap::iterator cur = it++;
	if ( cur->second -> is_nc() )
	{
	    try
	    {
		PortableServer::ObjectId_var id = the_poa->reference_to_id( cur->second -> nc() );
		the_poa->deactivate_object(id.in());
	    }
	    catch( PortableServer::POA::WrongAdapter & )
	    {
            	; // this is ok, we do not deactivate NON local naming context
            }
	}
	m_bindings.erase( cur );
    }
}

void
NamingContext_impl::doBind(Name & n, const boost::shared_ptr<Element> & elem, bool rebind)
{
    ERS_DEBUG( 1, "called with the '" << print_name( n ) << "' name" );

    //
    // Check if name is valid
    //
    if(n.length() == 0)
    {
        throw CosNaming::NamingContext::InvalidName();
    }

    if( n.length() == 1 )
    {
	{
	    boost::mutex::scoped_lock wl( m_mutex );

	    if ( rebind )
            {
		//
		// Do rebind
		//
		m_bindings[n[0].id._retn()] = elem;
            }
            else 
            {
		BindingMap::iterator it = m_bindings.find( n[0].id.in() );
		if( it != m_bindings.end() )
		{
		    //
		    // Name is already bound
		    //
		    throw CosNaming::NamingContext::AlreadyBound();
		}

		//
		// Do bind
		//
		m_bindings[n[0].id._retn()] = elem;
            }
            ++the_modifying_request_counter;
        }
    }
    else
    {
	boost::shared_ptr<Element> binding;
	{
	    boost::mutex::scoped_lock rl( m_mutex );
	    BindingMap::iterator it = m_bindings.find( n[0].id.in() );

	    if (it == m_bindings.end())
	    {
		//
		// Missing node 
		//
		throw CosNaming::NamingContext::NotFound(CosNaming::NamingContext::missing_node, n);
	    }
	
            binding = it->second;
	}

	//
	// Create the rest of the name
	//
	Name rest(n.length()-1,n.length()-1,n.get_buffer()+1,false);

	//
	// Let the next context handle the binding
	//
	if (binding -> nc_impl())
        {
	    binding -> nc_impl() -> doBind(rest, elem, rebind);
        }
        else
        {
            if (binding -> is_nc() == false)
	    {
		//
		// Binding must belong to a context
		//
		throw CosNaming::NamingContext::NotFound(CosNaming::NamingContext::not_context, rest);
	    }
            
	    if( elem -> is_nc() )
	    {
		if ( rebind )
		    binding -> nc() -> rebind_context(rest, elem -> nc());
		else
		    binding -> nc() -> bind_context(rest, elem -> nc());
	    }
	    else
	    {
		if ( rebind )
		    binding -> nc() -> rebind(rest, elem -> obj());
		else
		    binding -> nc() -> bind(rest, elem -> obj());
	    }
        }
    }
    ERS_DEBUG( 1, "success" );
}

void
NamingContext_impl::bind(const Name & n, CORBA::Object_ptr obj)
{
    ++the_global_request_counter;

    ERS_DEBUG( 1, "called with the '" << print_name( n ) << "' name" );
    boost::shared_ptr<Element> elem( new Element(obj) );
    doBind(const_cast<Name&>(n), elem, false);
    ERS_DEBUG( 1, "success" );
}

void
NamingContext_impl::rebind(const Name & n, CORBA::Object_ptr obj)
{
    ERS_DEBUG( 1, "called with the '" << print_name( n ) << "' name" );
    boost::shared_ptr<Element> elem( new Element(obj) );
    doBind(const_cast<Name&>(n), elem, true);
    ERS_DEBUG( 1, "success" );
}

void
NamingContext_impl::bind_context(const Name & n, NamingContext_ptr nc)
{
    ++the_global_request_counter;

    ERS_DEBUG( 1, "called with the '" << print_name( n ) << "' name" );
        
    boost::shared_ptr<Element> elem( new Element(nc) );
    doBind(const_cast<Name&>(n), elem, false);
    ERS_DEBUG( 1, "success" );
}

void
NamingContext_impl::rebind_context(const Name & n, NamingContext_ptr nc)
{
    ++the_global_request_counter;

    ERS_DEBUG( 1, "called with the '" << print_name( n ) << "' name" );
        
    boost::shared_ptr<Element> elem( new Element(nc) );
    doBind(const_cast<Name&>(n), elem, true);
    ERS_DEBUG( 1, "success" );
}

CORBA::Object_ptr
NamingContext_impl::resolve(const Name & n)
{
    ++the_global_request_counter;

    ERS_DEBUG( 1, "called with the '" << print_name( n ) << "' name" );
    
    //
    // Check if name is valid
    //
    if(n.length() == 0)
    {
	throw CosNaming::NamingContext::InvalidName();
    }

    //
    // Find binding
    //
    boost::shared_ptr<Element> binding;
    {
	boost::mutex::scoped_lock rl( m_mutex );
	BindingMap::iterator it = m_bindings.find( n[0].id.in() );
    
	if ( it == m_bindings.end() )
	{
	    throw CosNaming::NamingContext::NotFound(CosNaming::NamingContext::missing_node, n);
	}
        
	if(n.length() == 1)
	{
	    //
	    // Object or context found, return it
	    //
	    if(it->second->is_obj())
		return CORBA::Object::_duplicate(it->second->obj());
	    else
		return CosNaming::NamingContext::_duplicate(it->second->nc());
	}	

	binding = it->second;
    }
    
    //
    // Create the rest of the name
    //

    Name rest(n.length()-1,n.length()-1,const_cast<CosNaming::NameComponent*>( n.get_buffer() )+1,false);

    //
    // Let the next context handle the resolving of the name
    //
    if ( binding -> nc_impl() )
    {
	return binding -> nc_impl() -> resolve(rest);
    }
    else
    {
	if(binding -> is_nc() == false)
	{
	    //
	    // Binding must belong to a context
	    //
	    throw CosNaming::NamingContext::NotFound(CosNaming::NamingContext::not_context, rest);
	}
	return binding -> nc() -> resolve(rest);
    }
}

void
NamingContext_impl::unbind(const Name & n)
{
    ++the_global_request_counter;

    ERS_DEBUG( 1, "called with the '" << print_name( n ) << "' name" );

    //
    // Check if name is valid
    //
    if(n.length() == 0)
    {
	throw CosNaming::NamingContext::InvalidName();
    }
    
    if(n.length() == 1)
    {
	{
	    boost::mutex::scoped_lock wl( m_mutex );

	    BindingMap::iterator it = m_bindings.find( n[0].id.in() );

	    if ( it == m_bindings.end() )
	    {
		throw CosNaming::NamingContext::NotFound(CosNaming::NamingContext::missing_node, n);
	    }
	    
	    //
	    // Do unbind
	    //
	    m_bindings.erase( it );
            ++the_modifying_request_counter;
        }
    }
    else
    {
	//
	// Find binding
	//
	boost::shared_ptr<Element> binding;
	{
	    boost::mutex::scoped_lock rl( m_mutex );
	    BindingMap::iterator it = m_bindings.find( n[0].id.in() );

	    if ( it == m_bindings.end() )
	    {
		throw CosNaming::NamingContext::NotFound(CosNaming::NamingContext::missing_node, n);
	    }
	    
            binding = it->second;
	}
        
	//
	// Create the rest of the name
	//
	Name rest(n.length()-1,n.length()-1,const_cast<CosNaming::NameComponent*>( n.get_buffer() )+1,false);

	//
	// Let the next context handle the resolving of the name
	//
	if (binding -> nc_impl())
        {
	    binding -> nc_impl() -> unbind(rest);
        }
	else
	{
	    //
	    // Binding must belong to a context
	    //
	    if(binding -> is_nc() == false)
	    {
		throw CosNaming::NamingContext::NotFound(CosNaming::NamingContext::not_context, rest);
	    }
            binding -> nc() -> unbind(rest);
	}
    }
    ERS_DEBUG( 1, "success" );
}

NamingContext_impl *
NamingContext_impl::createNewContext( ) const
{
    ERS_DEBUG( 1, "enter" );
    NamingContext_impl * ni = new NamingContext_impl( );
    
    std::ostringstream out;
    out << ni;
    std::string servant_name = out.str();
    PortableServer::ObjectId_var oid = PortableServer::string_to_ObjectId( servant_name.c_str() );
    the_poa->activate_object_with_id( oid.in(), ni );
    ni->_remove_ref();
    
    ERS_DEBUG( 1, "success" );
    return ni;
}

NamingContext_ptr
NamingContext_impl::new_context()
{
    ++the_global_request_counter;

    ERS_DEBUG( 1, "enter" );
    NamingContext_impl * ni = createNewContext( );
    NamingContext_ptr nc = ni->_this();
    
    ERS_DEBUG( 1, "success" );
    return nc;
}

NamingContext_ptr
NamingContext_impl::bind_new_context(const Name & n)
{
    ++the_global_request_counter;

    ERS_DEBUG( 1, "called with the '" << print_name( n ) << "' name" );
    
    NamingContext_impl * ni = createNewContext( );
    NamingContext_var nc = ni->_this();
    try
    {
	boost::shared_ptr<Element> elem( new Element(nc,ni) );
	doBind(const_cast<Name&>(n), elem, false);
    }
    catch(...)
    {
    	nc->destroy();
	throw;
    }
    return nc._retn();
}

void
NamingContext_impl::destroy( )
{    
    ERS_DEBUG( 1, "enter" );
    if( m_bindings.size() > 0 )
    {
	throw CosNaming::NamingContext::NotEmpty();
    }

    PortableServer::ObjectId_var id = the_poa->servant_to_id(this);
    the_poa->deactivate_object(id.in());
        
    ERS_DEBUG( 1, "success" );
}

void 
NamingContext_impl::associate(const Name & n, const ipc::URI & uri)
{
    ++the_global_request_counter;

    ERS_DEBUG( 1, "called with the '" << print_name( n ) << "' name" );
    
    boost::shared_ptr<Element> elem( new Element(const_cast<ipc::URI &>(uri)) );
    doBind(const_cast<Name&>(n), elem, true);
    ERS_DEBUG( 1, "success" );
}
        
void
NamingContext_impl::obtain(const Name & n, ipc::URI_out uri)
{
    ++the_global_request_counter;

    ERS_DEBUG( 1, "called with the '" << print_name( n ) << "' name" );
    
    //
    // Check if name is valid
    //
    if(n.length() == 0)
    {
	throw CosNaming::NamingContext::InvalidName();
    }

    //
    // Find binding
    //
    boost::shared_ptr<Element> binding;
    {
	boost::mutex::scoped_lock rl( m_mutex );
	BindingMap::iterator it = m_bindings.find( n[0].id.in() );
    
	if ( it == m_bindings.end() )
	{
	    throw CosNaming::NamingContext::NotFound(CosNaming::NamingContext::missing_node, n);
	}
        
	if(n.length() == 1)
	{
	    //
	    // Object or context found, return it
	    //
	    uri = it->second->ref_dup();
	    return ;
	}	

	binding = it->second;
    }
    
    //
    // Create the rest of the name
    //

    Name rest(n.length()-1,n.length()-1,const_cast<CosNaming::NameComponent*>( n.get_buffer() )+1,false);

    //
    // Let the next context handle the resolving of the name
    //
    if ( binding -> nc_impl() )
    {
	return binding -> nc_impl() -> obtain(rest, uri);
    }
    else
    {
	throw CosNaming::NamingContext::NotFound(CosNaming::NamingContext::not_context, rest);
    }
}


void
NamingContext_impl::list( CORBA::ULong how_many,
			  BindingList_out bl,
			  BindingIterator_out bi )
{
    ++the_global_request_counter;
    
    ERS_DEBUG( 1, "enter" );
        
    boost::mutex::scoped_lock rl( m_mutex );
    const unsigned long len = m_bindings.size();
    const unsigned long num = len < how_many ? len : how_many;

    bl = new BindingList(num);
    bl -> length(num);
	         
    BindingMap::iterator it = m_bindings.begin( );

    for( unsigned long i = 0 ; i < num ; ++i, ++it )
    {	
	bl[i].binding_name.length( 1 );
	bl[i].binding_name[0].id = it->first.c_str();
	bl[i].binding_type = it->second->is_obj() ? nobject : ncontext;
    }

    if( how_many < len )
    {
	BindingList_var rest = new BindingList( len - how_many );
	rest -> length(len - how_many);

	for( unsigned long i = 0 ; i < len - how_many ; i++ )
	{
	    rest[i].binding_name.length( 1 );
	    rest[i].binding_name[0].id = it->first.c_str();
	    rest[i].binding_type = it->second->is_obj() ? nobject : ncontext;
	    it++;
	}

	BindingIterator_impl * bii = new BindingIterator_impl(rest);
	std::ostringstream out;
	out << bii;
	std::string servant_name = out.str();
	PortableServer::ObjectId_var oid = PortableServer::string_to_ObjectId( servant_name.c_str() );
	the_poa->activate_object_with_id( oid.in(), bii );
	bi = bii->_this();
	bii->_remove_ref();
    }
    else
    {
	bi = BindingIterator::_nil();
    }
    
    ERS_DEBUG( 1, "success" );
}

// ----------------------------------------------------------------------

BindingIterator_impl::BindingIterator_impl( BindingList_var & bl )
  : m_bl( bl ),
    m_idx( 0 )
{
}

bool
BindingIterator_impl::next_one( Binding_out b )
{
    ERS_DEBUG( 2, "enter" );
    b = new Binding;
    if( m_idx < m_bl -> length())
    {
	*b = m_bl[m_idx++]; 
	return true;
    }
    else
    {
	b->binding_type = nobject;
	return false;
    }
    ERS_DEBUG( 2, "success" );
}

bool
BindingIterator_impl::next_n( CORBA::ULong how_many, BindingList_out bl)
{
    ERS_DEBUG( 2, "enter" );
    unsigned long len = m_bl -> length();
    unsigned long num = (len - m_idx) < how_many ? (len - m_idx) : how_many;
    
    if ( num > 0 )
    {
	bl = new BindingList( num, num, m_bl->get_buffer() + m_idx, 0 );
	ERS_DEBUG( 2, "exit code = " << true );
	return true;
    }    
    else
    {
	ERS_DEBUG( 2, "exit code = " << false );
    	return false;
    }
}

void
BindingIterator_impl::destroy()
{
    ERS_DEBUG( 1, "enter" );
    PortableServer::ObjectId_var id = NamingContext_impl::the_poa->servant_to_id(this);
    NamingContext_impl::the_poa->deactivate_object(id.in());
    ERS_DEBUG( 1, "success" );
}

//////////////////////////////////////////////////////////////////////////
//
// Read/Write of the NamingContext
//
//////////////////////////////////////////////////////////////////////////
void
NamingContext_impl::write( std::ostream & out ) const
{    
    boost::mutex::scoped_lock rl( m_mutex );
    
    NamingContext_impl::BindingMap::const_iterator it = m_bindings.begin();

    unsigned int size = m_bindings.size();
    out.write( (const char*)&size, sizeof( size ) );

    for( ;it != m_bindings.end(); ++it )
    {
	if ( it->second->is_obj() )
	{
	    out <<= ObjectReference;
            out <<= it -> first;
	    out <<= it -> second->ref();
	}
	else
	{
	    if ( it->second->nc_impl() )
	    {
		out <<= LocalContext;
                out <<= it -> first;
		it->second->nc_impl()->write( out );
	    }
	    else
	    {
		// this is a remote naming context, save a reference to it
		out <<= RemoteContext;
                out <<= it -> first;
                out <<= it -> second -> ref();
		it -> second->write( out );
	    }
	}
    }
}

void
NamingContext_impl::read( std::istream & in )
{    
    boost::mutex::scoped_lock rl( m_mutex );
    
    unsigned int size;
    in.read( (char*)&size, sizeof( size ) );

    for( unsigned int i = 0; i < size; i++ )
    {
	Type type;
	String name;

	in >>= type;
	in >>= name;

	boost::shared_ptr<Element> element;
	if ( type == LocalContext )
	{
            NamingContext_impl * nci = createNewContext( );
	    nci -> read( in );
            NamingContext_var tmp = nci->_this();
	    element.reset( new Element( tmp, nci ) );
	}
	else
	{
	    ipc::URI uri;
	    in >>= uri;
	    if ( type == RemoteContext )
	    {
		CORBA::Object_var object = IPCCore::getORB() -> string_to_object( (const char *)uri.get_buffer() );
		NamingContext_var nc = CosNaming::NamingContext::_narrow( object );
		if ( !CORBA::is_nil( nc ) )
		{
		    element.reset( new Element( nc ) );
                }
                else
                {
                    ers::warning( InvalidBackupFile( ERS_HERE, "Invalid element type" ) );
                    continue;
                }
	    }
	    else if ( type == ObjectReference )
	    {
		element.reset( new Element( uri ) );
	    }
	    else
	    {
		throw InvalidBackupFile( ERS_HERE, "Invalid element type" );
	    }
	}
	m_bindings[name] = element;
    }
}
