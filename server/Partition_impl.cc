#include <errno.h>

#include <ipc/alarm.h>
#include <ipc/server/Partition_impl.h>
#include <ipc/server/backup.h>

namespace
{
  const unsigned int MAGIC_WORD = 0xfe01;
}

Partition_impl::Partition_impl(	const std::string & backup_dir,
                                unsigned long checkpoint_period,
                                unsigned long busy_threshold,
				bool do_restore,
				bool do_backup )
  : IPCNamedObject<POA_ipc::partition>( IPCPartition(), IPCPartition().name() ),
    m_do_backup( do_backup ),
    m_backup_directory( backup_dir ),
    m_alarm( 0 ),
    m_backup_file_name( backup_dir + "/" + partition().name() + ".backup" ),
    m_published( false ),
    m_checkpoint_period( checkpoint_period ),
    m_busy_threshold( busy_threshold )
{
    the_poa = _default_POA();
    
    if ( do_restore )
    	doRestore();
    
    if ( m_do_backup )
	m_alarm = new IPCAlarm( m_checkpoint_period, alarm_function, this );
}

Partition_impl::Partition_impl(	const std::string & name,
                                const std::string & backup_dir,
                                unsigned long checkpoint_period,
                                unsigned long busy_threshold,
				bool do_restore,
				bool do_backup )
  : IPCNamedObject<POA_ipc::partition,ipc::multi_thread,ipc::persistent>( IPCPartition(), name ),
    m_do_backup( do_backup ),
    m_backup_directory( backup_dir ),
    m_alarm( 0 ),
    m_backup_file_name( backup_dir + "/" + name + ".backup" ),
    m_published( false ),
    m_checkpoint_period( checkpoint_period ),
    m_busy_threshold( busy_threshold )
{
    the_poa = _default_POA();
    
    if ( do_restore )
    	doRestore();
        
    if ( m_do_backup )
	m_alarm = new IPCAlarm( m_checkpoint_period, alarm_function, this );
}

Partition_impl::~Partition_impl()
{
    if ( m_published )
    {
	try {
	    withdraw();
	}
	catch( daq::ipc::Exception & ex ) {
	    ers::error( ex );
	}
    }
    
    delete m_alarm;
    if ( m_do_backup )
    	doBackup();
}

void
Partition_impl::doBackup( )
{
    unsigned int total_requests = the_global_request_counter;
    unsigned int modifying_requests = the_modifying_request_counter;
    
    the_global_request_counter = 0;
    
    if ( modifying_requests == 0 )
    {
	ERS_DEBUG( 3, "Backup was not done because there were no changes since the last backup operation" );
    	return;
    }

    if ( ( total_requests/m_checkpoint_period ) > m_busy_threshold )
    {
	ERS_DEBUG( 3, "Backup was not done because the server is too busy (" 
        		<< total_requests << " in the last " << m_checkpoint_period << " second(s) )" );
    	return;
    }

    ERS_LOG( "There was " << modifying_requests << 
    	" modifying requests since the last backup, updating the backup ... " );

    the_modifying_request_counter = 0;
    
    try
    {
	bofstream fout( m_backup_file_name );
	fout.write( (const char*)&MAGIC_WORD, sizeof( MAGIC_WORD ) );
	write( fout );
	ERS_LOG( "backup file has been updated" );
    }
    catch( std::exception & ex )
    {
	ers::error( daq::ipc::CannotWriteFile( ERS_HERE, m_backup_file_name, ex.what() ) );
    }
}

void
Partition_impl::doRestore( )
{
    ERS_LOG( "Restoring information from the " << m_backup_file_name << " file ... " );
    try
    {
	bifstream fin( m_backup_file_name );

	unsigned int magic;
	fin.read( (char *)&magic, sizeof( MAGIC_WORD ) );
	if ( magic == MAGIC_WORD )
	{
	    read( fin );
	}
	else
	{
	    ers::error( daq::ipc::CannotReadFile( ERS_HERE, m_backup_file_name, "backup file is invalid or corrupted" ) );
	}
	ERS_LOG( "done" );
    }
    catch( CosNaming::InvalidBackupFile & ex )
    {
	ers::error( daq::ipc::CannotReadFile( ERS_HERE, m_backup_file_name, "backup file is invalid or corrupted", ex ) );
    }
    catch( std::exception & ex )
    {
	ers::error( daq::ipc::CannotReadFile( ERS_HERE, m_backup_file_name, ex.what() ) );
    }
}

bool
Partition_impl::alarm_function( void * rock )
{
    Partition_impl * partition = (Partition_impl*)rock;
    partition->doBackup( );
    return true;
}
